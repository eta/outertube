let
  sources = import ./npins/default.nix;
  pkgs = import sources."nixos-24.11".outPath {};
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    openssl.dev
    pkg-config
    clang
    mold 
  ];
}
