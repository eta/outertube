use serde::Deserialize;
use url::Url;

#[derive(Deserialize, Debug, Clone)]
pub struct Config {
    pub trackernet: TrackernetConfig,
    pub storage: StorageConfig,
    pub http: HttpConfig,
    #[serde(default)]
    pub processor: ProcessorConfig,
    #[serde(default)]
    pub force_journald: bool,
    #[serde(default)]
    pub clickhouse: Option<ClickhouseConfig>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct HttpConfig {
    pub listen: String,
    #[serde(default)]
    pub disable_api_key_checking: bool,
    #[serde(default)]
    pub api_keys: Vec<String>,
}

#[derive(Deserialize, Debug, Clone, Default)]
pub struct ProcessorConfig {
    #[serde(default)]
    pub descriptions_override_path: Option<String>,
    #[serde(default)]
    pub discard_observations: bool,
}

#[derive(Deserialize, Debug, Clone)]
pub struct StorageConfig {
    pub path: String,
    pub save_interval_sec: u64,
}

#[derive(Deserialize, Debug, Clone)]
pub struct TrackernetConfig {
    pub base_url: Url,
    pub requests_per_min: u64,
    pub app_key: String,
    pub request_timeout_sec: u64,
}

#[derive(Deserialize, Debug, Clone)]
pub struct ClickhouseConfig {
    pub base_url: Url,
    pub username: String,
    pub password: String,
}
