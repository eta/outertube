SELECT toUnixTimestamp(observation_ts) AS ts,
       observer,
       set_no,
       trip_no,
       lcid,
       train_id,
       track_code,
       destination_desc,
       location_desc,
       leading_car_no,
       line,
       intertube_filename,
       input_dest
FROM trains
WHERE intertube_filename = {filename:String}
ORDER BY ts ASC
FORMAT JSON