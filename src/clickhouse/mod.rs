//! Retrieving (and later storing?) train data from/to ClickHouse.

use crate::config::ClickhouseConfig;
use crate::processor::Train;
use crate::trackernet::TrackernetTrain;
use anyhow::{bail, Context};
use chrono::{DateTime, Duration};
use isahc::auth::{Authentication, Credentials};
use isahc::http::StatusCode;
use isahc::prelude::*;
use isahc::HttpClient;
use serde::{Deserialize, Deserializer};
use smol::io::AsyncReadExt;

/// Does queries with ClickHouse.
pub struct ClickhouseClient {
    http: HttpClient,
    config: ClickhouseConfig,
}

impl ClickhouseClient {
    pub fn new(config: ClickhouseConfig) -> anyhow::Result<Self> {
        let http = HttpClient::builder()
            .credentials(Credentials::new(
                config.username.clone(),
                config.password.clone(),
            ))
            .authentication(Authentication::basic())
            .build()?;

        Ok(Self { http, config })
    }

    pub async fn get_historical_train(
        &self,
        train_filename: &str,
    ) -> anyhow::Result<Option<Train>> {
        let mut url = self.config.base_url.clone();
        url.query_pairs_mut()
            .append_pair("param_filename", train_filename);

        let mut resp = self
            .http
            .post_async(url.as_str(), include_str!("./observations_for_train.sql"))
            .await
            .context("making clickhouse http request")?;

        let mut body = String::new();

        resp.body_mut()
            .read_to_string(&mut body)
            .await
            .context("reading clickhouse response body")?;

        if resp.status() != StatusCode::OK {
            bail!("clickhouse returned {}: {}", resp.status(), body);
        }

        let results: ClickhouseResponse<ClickhouseTrackernetTrain> =
            serde_json::from_str(&body).context("parsing clickhouse response body")?;

        let observations = results
            .data
            .into_iter()
            .map(|t| {
                (
                    DateTime::UNIX_EPOCH + Duration::seconds(t.ts as _),
                    TrackernetTrain::from(t),
                )
            })
            .collect::<Vec<_>>();

        if observations.is_empty() {
            Ok(None)
        } else {
            Ok(Some(Train::new_historical(observations)?))
        }
    }
}

fn deserialize_zeroable_u16<'de, D>(deserializer: D) -> Result<Option<u16>, D::Error>
where
    D: Deserializer<'de>,
{
    match u16::deserialize(deserializer) {
        Ok(0) => Ok(None),
        Ok(n) => Ok(Some(n)),
        Err(e) => Err(e),
    }
}

#[derive(Deserialize)]
struct ClickhouseTrackernetTrain {
    pub ts: u64,
    pub observer: String,
    #[serde(deserialize_with = "deserialize_zeroable_u16")]
    pub set_no: Option<u16>,
    #[serde(deserialize_with = "deserialize_zeroable_u16")]
    pub trip_no: Option<u16>,
    #[serde(deserialize_with = "deserialize_zeroable_u16")]
    pub lcid: Option<u16>,
    pub train_id: String,
    pub track_code: String,
    pub destination_desc: String,
    pub location_desc: String,
    pub leading_car_no: u32,
    pub line: String,
    pub intertube_filename: String,
    pub input_dest: String,
}

impl From<ClickhouseTrackernetTrain> for TrackernetTrain {
    fn from(ch: ClickhouseTrackernetTrain) -> Self {
        Self {
            train_id: ch.train_id,
            lcid: ch.lcid,
            set_no: ch.set_no,
            trip_no: ch.trip_no,
            headcode: None,
            location_desc: Some(ch.location_desc),
            overridden_location_desc: None,
            destination_desc: ch.destination_desc,
            track_code: ch.track_code,
            line_code: ch.line,
            leading_car_no: ch.leading_car_no.to_string(),
            input_dest: Some(ch.input_dest),
            dest_code: None,
            seconds_to: None,
            time_to: None,
            order: None,
            arrival_time: None,
            depart_time: None,
            depart_interval: None,
            departed: None,
            direction: None,
            is_stalled: None,
        }
    }
}

#[derive(Deserialize)]
#[allow(dead_code)]
struct ClickhouseStatistics {
    elapsed: f64,
    rows_read: u64,
    bytes_read: u64,
}

#[derive(Deserialize)]
#[allow(dead_code)]
struct ClickhouseResponse<T> {
    data: Vec<T>,
    rows: usize,
    statistics: ClickhouseStatistics,
}
