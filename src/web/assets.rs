use crate::web::State;
use tide::http::mime;
use tide::{Response, Server};

static CSS: &str = grass::include!("./web/css/main.scss");

macro_rules! asset {
    ($web:ident, $path:expr, $mime:expr) => {
        $web.at(concat!("/", $path)).get(move |_| async {
            Ok(Response::builder(200)
                .header("Cache-Control", "max-age=604800, immutable")
                .body(include_bytes!(concat!("../../web/assets/", $path)) as &[u8])
                .content_type($mime)
                .build())
        })
    };
}

pub(crate) fn add_assets(web: &mut Server<State>) {
    web.at("/styles.css").get(move |_| async {
        Ok(Response::builder(200)
            .header("Cache-Control", "max-age=604800, immutable")
            .body(CSS)
            .content_type(mime::CSS)
            .build())
    });
    asset!(web, "logo.svg", mime::SVG);
    asset!(web, "android-chrome-192x192.png", mime::PNG);
    asset!(web, "android-chrome-512x512.png", mime::PNG);
    asset!(web, "apple-touch-icon.png", mime::PNG);
    asset!(web, "favicon-16x16.png", mime::PNG);
    asset!(web, "favicon-32x32.png", mime::PNG);
    asset!(web, "mstile-150x150.png", mime::PNG);
    asset!(web, "safari-pinned-tab.svg", mime::SVG);
    asset!(web, "logo.svg", mime::SVG);
    asset!(web, "logo-text.svg", mime::SVG);
    asset!(web, "logo-text-cracked.svg", mime::SVG);
    asset!(web, "browserconfig.xml", mime::XML);
    asset!(web, "favicon.ico", mime::ICO);
    asset!(web, "site.webmanifest", mime::PLAIN);
    // Inter font family
    asset!(web, "fonts/Inter-BlackItalic.woff", "font/woff");
    asset!(web, "fonts/Inter-BlackItalic.woff2", "font/woff2");
    asset!(web, "fonts/Inter-Black.woff", "font/woff");
    asset!(web, "fonts/Inter-Black.woff2", "font/woff2");
    asset!(web, "fonts/Inter-BoldItalic.woff", "font/woff");
    asset!(web, "fonts/Inter-BoldItalic.woff2", "font/woff2");
    asset!(web, "fonts/Inter-Bold.woff", "font/woff");
    asset!(web, "fonts/Inter-Bold.woff2", "font/woff2");
    asset!(web, "fonts/Inter-ExtraBoldItalic.woff", "font/woff");
    asset!(web, "fonts/Inter-ExtraBoldItalic.woff2", "font/woff2");
    asset!(web, "fonts/Inter-ExtraBold.woff", "font/woff");
    asset!(web, "fonts/Inter-ExtraBold.woff2", "font/woff2");
    asset!(web, "fonts/Inter-ExtraLightItalic.woff", "font/woff");
    asset!(web, "fonts/Inter-ExtraLightItalic.woff2", "font/woff2");
    asset!(web, "fonts/Inter-ExtraLight.woff", "font/woff");
    asset!(web, "fonts/Inter-ExtraLight.woff2", "font/woff2");
    asset!(web, "fonts/Inter-italic.var.woff2", "font/woff2");
    asset!(web, "fonts/Inter-Italic.woff", "font/woff");
    asset!(web, "fonts/Inter-Italic.woff2", "font/woff2");
    asset!(web, "fonts/Inter-LightItalic.woff", "font/woff");
    asset!(web, "fonts/Inter-LightItalic.woff2", "font/woff2");
    asset!(web, "fonts/Inter-Light.woff", "font/woff");
    asset!(web, "fonts/Inter-Light.woff2", "font/woff2");
    asset!(web, "fonts/Inter-MediumItalic.woff", "font/woff");
    asset!(web, "fonts/Inter-MediumItalic.woff2", "font/woff2");
    asset!(web, "fonts/Inter-Medium.woff", "font/woff");
    asset!(web, "fonts/Inter-Medium.woff2", "font/woff2");
    asset!(web, "fonts/Inter-Regular.woff", "font/woff");
    asset!(web, "fonts/Inter-Regular.woff2", "font/woff2");
    asset!(web, "fonts/Inter-roman.var.woff2", "font/woff2");
    asset!(web, "fonts/Inter-SemiBoldItalic.woff", "font/woff");
    asset!(web, "fonts/Inter-SemiBoldItalic.woff2", "font/woff2");
    asset!(web, "fonts/Inter-SemiBold.woff", "font/woff");
    asset!(web, "fonts/Inter-SemiBold.woff2", "font/woff2");
    asset!(web, "fonts/Inter-ThinItalic.woff", "font/woff");
    asset!(web, "fonts/Inter-ThinItalic.woff2", "font/woff2");
    asset!(web, "fonts/Inter-Thin.woff", "font/woff");
    asset!(web, "fonts/Inter-Thin.woff2", "font/woff2");
    asset!(web, "fonts/Inter.var.woff2", "font/woff2");
}
