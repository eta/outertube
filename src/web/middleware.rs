use crate::web::templates::{BaseTemplate, ErrorTemplate};
use crate::web::State;
use slog::{info, warn, Logger};
use std::time::Instant;
use tide::utils::async_trait;
use tide::{Middleware, Next, Request, Response};

pub struct Wrapper {
    pub(crate) log: Logger,
}

#[async_trait]
impl Middleware<State> for Wrapper {
    async fn handle(&self, req: Request<State>, next: Next<'_, State>) -> tide::Result {
        let start = Instant::now();
        let path = req.url().path().to_owned();
        let remote = req.remote().unwrap_or("???").to_owned();
        let response = next.run(req).await;
        let time = Instant::now().duration_since(start);

        crate::metrics::HTTP_REQUESTS_COUNTER
            .with_label_values(&[&(response.status() as u16).to_string()])
            .inc();

        if let Some(e) = response.error() {
            warn!(
                self.log,
                "error in web request";
                "path" => path,
                "status" => response.status().to_string(),
                "remote" => remote,
                "error" => format!("{e:#?}"),
                "msec" => time.as_millis()
            );
            Ok(askama_tide::into_response(&ErrorTemplate {
                title: e.status().canonical_reason().into(),
                status_code: e.status().into(),
                text: None,
                base: BaseTemplate {
                    title: e.status().canonical_reason().into(),
                    accent_colour: "#ff4f00", // International Orange
                    ..Default::default()
                },
            }))
        } else {
            info!(
                self.log,
                "handled web request";
                "path" => path,
                "status" => response.status().to_string(),
                "remote" => remote,
                "msec" => time.as_millis()
            );
            Ok(response)
        }
    }
}

pub struct ApiAuth;
#[async_trait]
impl Middleware<State> for ApiAuth {
    async fn handle(&self, req: Request<State>, next: Next<'_, State>) -> tide::Result {
        if req.url().path().starts_with("/api/") {
            if let Some(bearer_token) = req
                .header("Authorization")
                .and_then(|x| x.last().as_str().strip_prefix("Bearer "))
            {
                if req.state().api_keys.contains(bearer_token) {
                    Ok(next.run(req).await)
                } else {
                    Ok(Response::builder(401).body("invalid bearer token").build())
                }
            } else {
                Ok(Response::builder(401)
                    .body("no bearer token provided")
                    .build())
            }
        } else {
            Ok(next.run(req).await)
        }
    }
}
