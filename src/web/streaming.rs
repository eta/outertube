//! Streaming train data between instances of outertube.

use async_broadcast::Receiver;
use futures::StreamExt;
use serde::Serialize;
use smol::io::{AsyncBufRead, AsyncRead};
use std::io;
use std::io::{Cursor, Read};
use std::pin::Pin;
use std::task::{Context, Poll};

/// Converts a stream of objects from a broadcast channel into an `AsyncRead` /
/// `AsyncBufRead` that provides jsonl for each object.
pub struct JsonlStreamer<T, F>
where
    F: FnOnce() + Unpin,
{
    /// Where the trains come from.
    rx: Receiver<T>,
    /// A space to hold a singular serialized `TrackernetTrain` plus newline.
    buf: Cursor<Vec<u8>>,
    /// Something to run on connection close.
    on_close: Option<F>,
}

impl<T, F> Drop for JsonlStreamer<T, F>
where
    F: FnOnce() + Unpin,
{
    fn drop(&mut self) {
        if let Some(f) = self.on_close.take() {
            f();
        }
    }
}

impl<T, F> JsonlStreamer<T, F>
where
    T: Serialize + Clone,
    F: FnOnce() + Unpin,
{
    pub fn new(rx: Receiver<T>, on_close: F) -> Self {
        Self {
            rx,
            buf: Cursor::new(Vec::new()),
            on_close: Some(on_close),
        }
    }
    /// Refill the internal buffer with more train data.
    ///
    /// If `Ok(true)` is returned, the buffer was refilled. `false` means we should return
    /// `Pending`.
    fn refill(&mut self, cx: &mut Context<'_>) -> io::Result<bool> {
        self.buf.get_mut().clear();
        self.buf.set_position(0);
        match self.rx.poll_next_unpin(cx) {
            Poll::Ready(Some(obj)) => {
                serde_json::to_writer(self.buf.get_mut(), &obj)?;
                self.buf.get_mut().extend_from_slice(b"\r\n");
                Ok(true)
            }
            Poll::Ready(None) => Err(io::Error::new(
                io::ErrorKind::BrokenPipe,
                "train channel closed",
            )),
            Poll::Pending => Ok(false),
        }
    }
}

impl<T, F> AsyncRead for JsonlStreamer<T, F>
where
    T: Serialize + Clone,
    F: FnOnce() + Unpin,
{
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<io::Result<usize>> {
        if buf.is_empty() {
            return Poll::Ready(Ok(0));
        }
        // We serialize each train from `rx` into `self.buf`, and then copy from there into
        // `buf` (using `Cursor`).
        let mut count = 0;
        loop {
            // See if there's any data left to copy.
            match self.buf.read(&mut buf[count..]).unwrap() {
                0 => {
                    // We don't have anything in the buffer, so reset the buffer and get a train
                    // to serialize into it.
                    match self.refill(cx) {
                        Ok(true) => {
                            // The buffer was refilled; loop to copy the bytes.
                        }
                        Ok(false) => {
                            return if count > 0 {
                                Poll::Ready(Ok(count))
                            } else {
                                Poll::Pending
                            };
                        }
                        Err(e) => return Poll::Ready(Err(e)),
                    }
                }
                n => {
                    // We wrote some bytes. If they're enough, exit now; else, loop to refill the
                    // buffer.
                    count += n;
                    if count == buf.len() {
                        return Poll::Ready(Ok(count));
                    }
                }
            }
        }
    }
}

impl<T, F> AsyncBufRead for JsonlStreamer<T, F>
where
    T: Serialize + Clone,
    F: FnOnce() + Unpin,
{
    fn poll_fill_buf(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<&[u8]>> {
        if self.buf.get_ref().len() == self.buf.position() as usize {
            match self.refill(cx) {
                Ok(true) => {}
                Ok(false) => return Poll::Pending,
                Err(e) => return Poll::Ready(Err(e)),
            }
        }
        let pos = self.buf.position() as usize;
        Poll::Ready(Ok(&self.get_mut().buf.get_ref()[pos..]))
    }

    fn consume(mut self: Pin<&mut Self>, amt: usize) {
        let new_pos = self.buf.position() + amt as u64;
        self.buf.set_position(new_pos);
    }
}
