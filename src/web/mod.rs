//! It's a website???

use crate::config::HttpConfig;
use crate::routes::RouteFile;
use crate::storage::{TrainData, TrainDataInner};
use crate::web::api::train;
use crate::web::lines::{get_line_by_code, ALL_LINES};
use crate::web::live_routes::{InactiveRouteStreamerHandle, RouteStreamer};
use crate::web::live_train::TrainStreamer;
use crate::web::middleware::{ApiAuth, Wrapper};
use crate::web::templates::{
    BaseTemplate, ChooseTrainTemplate, LineOverviewBasicTemplate, LineOverviewModelTemplate,
    RootTemplate, TrainJourneyTemplate, TrainTemplate,
};
use anyhow::anyhow;
use serde::Deserialize;
use slog::{error, warn, Logger};
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, Mutex, RwLockReadGuard};
use std::thread;
use tide::Request;

mod api;
mod assets;
mod lines;
mod live_routes;
mod live_train;
mod middleware;
mod streaming;
mod templates;

#[derive(Clone)]
struct State {
    api_keys: HashSet<String>,
    train_data: Arc<TrainData>,
    log: Logger,
    route_streamers: Arc<Mutex<HashMap<(String, String), InactiveRouteStreamerHandle>>>,
}

impl State {
    fn train_data(&self) -> RwLockReadGuard<TrainDataInner> {
        self.train_data.inner.read().unwrap()
    }
}

async fn root(_: Request<State>) -> tide::Result {
    Ok(askama_tide::into_response(&RootTemplate {
        base: BaseTemplate::default(),
        lines: ALL_LINES,
    }))
}

#[derive(Deserialize)]
struct TrainQuery {
    #[serde(default)]
    journey: Option<bool>,
}

async fn web_train(req: Request<State>) -> tide::Result {
    let tid = req.param("tid")?;
    let trip = req.param("trip").ok().and_then(|x| x.parse::<usize>().ok());
    let TrainQuery { journey } = req.query()?;
    let data = req.state().train_data();
    if let Some(train) = data.trains.get(tid) {
        if journey.unwrap_or(true) || trip.is_some() {
            if let Some(t) = trip {
                if train.trips().get(t).is_none() {
                    return Err(tide::Error::new(
                        404,
                        anyhow!(
                            "train {} doesn't have a trip with id {}",
                            train.unique_id(),
                            t
                        ),
                    ));
                }
            }
            Ok(askama_tide::into_response(&TrainJourneyTemplate::new(
                train, trip,
            )))
        } else {
            Ok(askama_tide::into_response(&TrainTemplate::new(train)))
        }
    } else {
        Err(tide::Error::new(
            404,
            anyhow!("could not find train {}", tid),
        ))
    }
}

async fn web_historical_train(req: Request<State>) -> tide::Result {
    let tid = req.param("tid")?;
    let log = &req.state().log;

    if let Some(ch) = req.state().train_data.clickhouse.as_ref() {
        let train = match ch.get_historical_train(tid).await {
            Ok(Some(t)) => t,
            Ok(None) => {
                return Err(tide::Error::new(404, anyhow!("train not found")));
            }
            Err(e) => {
                return Err(tide::Error::new(
                    500,
                    anyhow!("clickhouse query failed: {e:?}"),
                ));
            }
        };
        Ok(askama_tide::into_response(&TrainTemplate::new(&train)))
    } else {
        Err(tide::Error::new(404, anyhow!("clickhouse not configured")))
    }
}

#[derive(Deserialize)]
struct LineQuery {
    #[serde(default)]
    route: Option<String>,
    #[serde(default)]
    fallback: bool,
}

async fn web_line(req: Request<State>) -> tide::Result {
    let line = req.param("line")?;
    let LineQuery { route, fallback } = req.query()?;
    let line_data =
        get_line_by_code(line).ok_or(tide::Error::new(404, anyhow!("no such line {line}")))?;
    let data = req.state().train_data();
    if let Some(route_file) = (!fallback).then(|| RouteFile::for_line(line)).flatten() {
        Ok(askama_tide::into_response(&LineOverviewModelTemplate::new(
            line_data,
            &data,
            route_file,
            route.as_deref(),
        )))
    } else {
        Ok(askama_tide::into_response(&LineOverviewBasicTemplate::new(
            line_data, &data,
        )))
    }
}

async fn web_stream_train(req: Request<State>) -> tide::Result {
    let train_id = req.param("tid")?.to_owned();
    let trip = req
        .param("trip")?
        .parse::<usize>()
        .map_err(|_| tide::Error::new(400, anyhow!("invalid trip param")))?;

    let log = req.state().log.clone();
    let train_data = req.state().train_data.clone();

    Ok(tide::sse::upgrade(req, move |_req, sender| {
        let train_data = train_data.clone();
        let train_id = train_id.clone();
        let log = log.clone();
        async move {
            TrainStreamer::run(train_data, train_id, trip, sender, &log).await;
            Ok(())
        }
    }))
}

async fn web_stream_route(req: Request<State>) -> tide::Result {
    let line = req.param("line")?;
    let line_data =
        get_line_by_code(line).ok_or(tide::Error::new(404, anyhow!("no such line {line}")))?;
    let route_id = req.param("route")?;
    let route = RouteFile::for_line(line)
        .and_then(|rf| rf.route(route_id))
        .ok_or(tide::Error::new(
            404,
            anyhow!("no such line/route {line}/{route_id}"),
        ))?;

    let key = (line.to_owned(), route_id.to_owned());
    let mut streamers_map = req.state().route_streamers.lock().unwrap();

    // We reactivate to check it's fresh... we then have to deactivate because the tide sse thing
    // is a `Fn` when it should be a `FnOnce` (sigh!), so requires cloning.
    let streamer = if let Some(existing) = streamers_map.remove(&key).and_then(|e| e.activate()) {
        existing.deactivate()
    } else {
        RouteStreamer::start(
            line_data,
            route,
            req.state().train_data.clone(),
            &req.state().log,
        )?
        .deactivate()
    };
    // Store a copy for later.
    streamers_map.insert(key, streamer.clone());
    drop(streamers_map);

    let log = req.state().log.clone();

    Ok(tide::sse::upgrade(req, move |_req, sender| {
        let streamer_ = streamer.clone().activate();
        let log_ = log.clone();
        async move {
            if let Some(mut streamer_) = streamer_ {
                let initial = streamer_.serialized_initial_state()?;
                sender.send("initial", &initial, None).await?;
                loop {
                    match streamer_.next_update().await {
                        Ok(next) => {
                            sender.send("update", &next, None).await?;
                        }
                        Err(e) => {
                            // Probably this is due to overflow, in which case closing the
                            // connection will let the client resync.
                            warn!(log_, "failed to read route streamer update: {e:?}");
                        }
                    }
                }
            } else {
                Err(anyhow!("route sender mysteriously disappeared"))?
            }
        }
    }))
}

async fn web_choose_train(req: Request<State>) -> tide::Result {
    let payload = req.param("payload")?;
    let data = req.state().train_data();
    Ok(askama_tide::into_response(&ChooseTrainTemplate::new(
        &data, payload,
    )?))
}

pub(crate) fn start_web(config: HttpConfig, train_data: Arc<TrainData>, log: Logger) {
    let listen = config.listen.clone();
    let mut web = tide::with_state(State {
        train_data,
        api_keys: config.api_keys.into_iter().collect(),
        log: log.clone(),
        route_streamers: Arc::new(Mutex::new(HashMap::new())),
    });
    assets::add_assets(&mut web);
    web.with(Wrapper { log: log.clone() });
    if config.disable_api_key_checking {
        error!(
            log,
            "not doing any API key checking! I hope this isn't production"
        );
    } else {
        web.with(ApiAuth);
    }
    web.at("/").get(root);
    web.at("/train/:tid").get(web_train);
    web.at("/train/:tid/sse/:trip").get(web_stream_train);
    web.at("/train/:tid/trip/:trip").get(web_train);
    web.at("/choose-train/:payload").get(web_choose_train);
    web.at("/historical/:tid").get(web_historical_train);
    web.at("/line/:line").get(web_line);
    web.at("/line/:line/sse/:route").get(web_stream_route);
    web.at("/metrics").get(api::metrics);
    web.at("/api/v1/trains").get(api::trains);
    web.at("/api/v1/firehose").get(api::firehose);
    web.at("/api/v1/trains/:tid").get(api::train);
    web.at("/api/v1/routes/:line/:route").get(api::filled_route);
    web.at("/api/v1/routes/:line/:route/empty")
        .get(api::empty_route);
    thread::spawn(move || {
        if let Err(e) = smol::block_on(web.listen(&listen)) {
            panic!("webserver died: {e}");
        }
    });
}
