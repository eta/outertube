//! Streaming live updates to clients about trains.

use crate::processor::{HistoryEventContent, Location, LocationKind, ProcessorEvent, Train};
use crate::storage::TrainData;
use crate::web::lines::get_line_by_code;
use crate::web::templates::JourneyPointTemplate;
use anyhow::{bail, Context};
use askama::Template;
use either::Either;
use serde::Serialize;
use slog::{debug, info, o, warn, Logger};
use std::borrow::Cow;
use std::sync::Arc;
use thiserror::Error;
use tide::sse;

pub fn train_journey_points(
    train: &Train,
    trip_id: Option<usize>,
) -> Vec<JourneyPointTemplate<'_>> {
    let accent_colour = get_line_by_code(train.line_code())
        .map(|x| x.html_colour)
        .unwrap_or("black");
    let current_trip = trip_id.and_then(|t| train.trips().get(t));
    let trip_id = trip_id.unwrap_or_else(|| train.current_trip_id());
    let current_loc_idx = train.locations().len().saturating_sub(1);

    let mut locations = train
        .locations()
        .iter()
        .enumerate()
        .filter(|(_, loc)| loc.trip_id.contains(&trip_id))
        .map(move |(i, loc)| JourneyPointTemplate {
            kind: Cow::Borrowed(&loc.kind),
            is_future: false,
            is_current: i == current_loc_idx,
            is_gap: false,
            is_first: false,
            is_last: false,
            start_ts: Some(&loc.start_ts),
            end_ts: Some(&loc.end_ts),
            accent_colour,
        })
        .chain(
            if current_trip.is_none() {
                Either::Left(train.future_locations().iter())
            } else {
                Either::Right(std::iter::empty())
            }
            .map(move |loc| JourneyPointTemplate::future(Cow::Borrowed(&loc.kind), accent_colour)),
        )
        .collect::<Vec<_>>();

    if current_trip.is_none() && train.future_locations().is_empty() && !train.at_destination() {
        // If we're between stations, at least say what the next station is like intertube
        // used to do.
        if train
            .current_location()
            .map(|x| matches!(x.kind, LocationKind::Transit { .. }))
            .unwrap_or(false)
        {
            if let Some(next) = train.next_location_kind() {
                locations.push(JourneyPointTemplate::future(next, accent_colour));
            }
        }
        locations.push(JourneyPointTemplate {
            kind: Cow::Borrowed(&LocationKind::EMPTY_TRANSIT),
            is_future: false,
            is_current: false,
            is_gap: true,
            is_first: false,
            is_last: false,
            start_ts: None,
            end_ts: None,
            accent_colour,
        });
        locations.push(JourneyPointTemplate::future(
            train
                .destination()
                .location_kind
                .as_ref()
                .map(Cow::Borrowed)
                .unwrap_or_else(|| {
                    Cow::Owned(LocationKind::Station {
                        name: train.destination().text.clone(),
                        platform: None,
                    })
                }),
            accent_colour,
        ))
    }

    if let Some(l) = locations.first_mut() {
        l.is_first = true;
    }
    if let Some(l) = locations.last_mut() {
        l.is_last = true;
    }

    locations
}

pub struct TrainStreamer {
    log: Logger,
    train_data: Arc<TrainData>,
    templated_journey_points: Vec<String>,
    train_id: String,
    current_trip_id: usize,
    rx: async_broadcast::Receiver<ProcessorEvent>,
    tx: sse::Sender,
}

#[derive(Serialize)]
struct PointUpdate<'a> {
    point_id: usize,
    html: &'a str,
}

#[derive(Serialize)]
struct Update<'a> {
    points: Vec<PointUpdate<'a>>,
    length: usize,
}

static EMPTY_STRING: String = String::new();

impl TrainStreamer {
    pub async fn run(
        train_data: Arc<TrainData>,
        train_id: String,
        trip_id: usize,
        sender: sse::Sender,
        log: &Logger,
    ) {
        let rx = train_data.events_rx.activate_cloned();

        Self {
            log: log.new(o!("train_id" => train_id.clone(), "trip" => trip_id)),
            train_data,
            templated_journey_points: vec![],
            train_id,
            current_trip_id: trip_id,
            rx,
            tx: sender,
        }
        .run_inner()
        .await
    }

    async fn run_inner(mut self) {
        // Send the initial batch of updates (which could fail straight away)
        if self.process_and_communicate_updates().await.is_err() {
            return;
        }
        info!(self.log, "starting train streamer");
        while let Ok(event) = self.rx.recv_direct().await {
            let is_ours = match &event {
                ProcessorEvent::New { .. } => false,
                ProcessorEvent::Update { train_id, .. } => train_id == &self.train_id,
                ProcessorEvent::Archive { train_id } => train_id == &self.train_id,
                ProcessorEvent::Rescue {
                    train_id,
                    target_id,
                } => train_id == &self.train_id || target_id == &self.train_id,
            };
            if is_ours {
                // Send an update; if it fails, exit.
                if self.process_and_communicate_updates().await.is_err() {
                    return;
                }
            }
        }
        info!(
            self.log,
            "train streamer shutting down due to channel error",
        );
    }
    async fn process_and_communicate_updates(&mut self) -> anyhow::Result<()> {
        match self.process_updates().await {
            Ok(u) => {
                self.tx.send("update", u, None).await?;
                Ok(())
            }
            Err(UpdateProcessingError::TrainGone) => {
                info!(self.log, "train streamer's train went away");

                self.tx.send("cease", "train-gone", None).await?;
                bail!("streamer ceased")
            }
            Err(UpdateProcessingError::NewTrip) => {
                info!(self.log, "train streamer encountered a new trip");

                self.tx.send("cease", "new-trip", None).await?;
                bail!("streamer ceased")
            }
            Err(e) => {
                warn!(self.log, "failed to process streamer updates: {e}");

                Err(e.into())
            }
        }
    }
    async fn process_updates(&mut self) -> Result<String, UpdateProcessingError> {
        let data = self.train_data.inner.read().unwrap();

        // Check that we should still be streaming.

        let train = data
            .trains
            .get(&self.train_id)
            .ok_or(UpdateProcessingError::TrainGone)?;

        if train.current_trip_id() != self.current_trip_id {
            return Err(UpdateProcessingError::NewTrip);
        }

        let journey_points_templated = train_journey_points(train, None)
            .into_iter()
            .map(|p| p.render())
            .collect::<Result<Vec<_>, _>>()?;

        drop(data);

        // We need to make our comparison infinite length, so if the lengths don't match
        // we still iterate everything in the new version.
        let previous_journey_points = self
            .templated_journey_points
            .iter()
            .chain(std::iter::repeat(&EMPTY_STRING));

        let points = previous_journey_points
            .zip(journey_points_templated.iter())
            .enumerate()
            .filter(|(_, (old, new))| old != new)
            .map(|(id, (_old, new))| PointUpdate {
                point_id: id,
                html: new,
            })
            .collect::<Vec<_>>();

        let payload = serde_json::to_string(&Update {
            points,
            length: journey_points_templated.len(),
        })?;

        self.templated_journey_points = journey_points_templated;

        Ok(payload)
    }
}

#[derive(Error, Debug)]
enum UpdateProcessingError {
    #[error("train disappeared")]
    TrainGone,
    #[error("trip id is no longer current")]
    NewTrip,
    #[error("rendering failed: {0}")]
    Render(#[from] askama::Error),
    #[error("serialize failed: {0}")]
    Serde(#[from] serde_json::Error),
}
