use crate::processor::{HistoryEvent, HistoryEventContent, Location, LocationKind, Train, Trip};
use crate::routes::{Route, RouteFile, RouteGroup, RoutePoint};
use crate::storage::TrainDataInner;
use crate::web::lines::{get_line_by_code, LineData};
use crate::web::live_routes::{get_filled_route, FilledRoutePoint, FilledRouteResults};
use crate::web::live_train::train_journey_points;
use anyhow::anyhow;
use askama::Template;
use chrono::{DateTime, Timelike, Utc};
use chrono_tz::Europe;
use std::borrow::Cow;
use std::cmp;
use std::collections::HashMap;
use std::fmt::Display;
use std::ops::Sub;
use std::rc::Rc;

pub struct BaseTemplate<'a> {
    pub title: Cow<'a, str>,
    pub server_version: &'static str,
    pub updated: String,
    pub accent_colour: &'static str,
    pub scroll_snap: bool,
}

impl Default for BaseTemplate<'_> {
    fn default() -> Self {
        Self {
            title: "outertube".into(),
            server_version: env!("CARGO_PKG_VERSION"),
            updated: Utc::now()
                .with_timezone(&Europe::London)
                .format("%H:%M:%S")
                .to_string(),
            accent_colour: "#1d70b8",
            scroll_snap: false,
        }
    }
}

#[derive(Template)]
#[template(path = "root.html")]
pub struct RootTemplate<'a> {
    pub base: BaseTemplate<'a>,
    pub lines: &'static [LineData],
}

#[derive(Clone, Template)]
#[template(path = "journey-point.html")]
pub struct JourneyPointTemplate<'a> {
    pub kind: Cow<'a, LocationKind>,
    pub is_future: bool,
    pub is_gap: bool,
    pub is_current: bool,
    pub is_first: bool,
    pub is_last: bool,
    pub start_ts: Option<&'a DateTime<Utc>>,
    pub end_ts: Option<&'a DateTime<Utc>>,
    pub accent_colour: &'static str,
}

impl<'a> JourneyPointTemplate<'a> {
    pub(crate) fn future(kind: Cow<'a, LocationKind>, accent_colour: &'static str) -> Self {
        Self {
            kind,
            is_future: true,
            is_gap: false,
            is_current: false,
            is_first: false,
            is_last: false,
            start_ts: None,
            end_ts: None,
            accent_colour,
        }
    }
    fn kind(&self) -> &LocationKind {
        &self.kind
    }
    fn progress_perc_rounded(&self) -> usize {
        if let LocationKind::Transit {
            progress_perc: Some(p),
            ..
        } = *self.kind
        {
            p.round() as usize
        } else {
            0
        }
    }
    fn live_timing_arr(&self) -> Option<impl Display> {
        if self.is_future {
            None
        } else {
            self.start_ts
                .map(|ts| ts.with_timezone(&Europe::London).format("%H:%M"))
        }
    }
    fn live_timing_dep(&self) -> Option<String> {
        if self.is_future {
            None
        } else if self.is_current {
            if let (Some(start), Some(end)) = (self.start_ts, self.end_ts) {
                let dur = end.signed_duration_since(start);
                Some(if dur.num_seconds() >= 60 {
                    format!("+{}m", dur.num_minutes())
                } else {
                    format!("+{}s", dur.num_seconds())
                })
            } else {
                // shouldn't really get here
                None
            }
        } else {
            self.end_ts.map(|ts| {
                ts.with_timezone(&Europe::London)
                    .format("%H:%M")
                    .to_string()
            })
        }
    }
}

#[derive(Template)]
#[template(path = "train-journey.html")]
pub struct TrainJourneyTemplate<'a> {
    pub base: BaseTemplate<'a>,
    pub train: &'a Train,
    pub locations: Vec<JourneyPointTemplate<'a>>,
    pub current_trip: Option<&'a Trip>,
    pub previous_trip: Option<(usize, &'a Trip)>,
    pub next_trip: Option<(usize, &'a Trip)>,
    pub next_trip_is_current: bool,
    pub trip_id: usize,
    pub title: String,
}

fn make_journey_title(train: &Train, trip: Option<&Trip>) -> String {
    let (train_number, destination_text) = if let Some(t) = trip {
        (t.train_number, t.end.station_name().unwrap_or("somewhere"))
    } else {
        (train.train_number(), &*train.destination().text)
    };

    if let Some(n) = train_number {
        format!("#{n:03} to {}", destination_text)
    } else {
        format!("Train to {}", destination_text)
    }
}

impl<'a> TrainJourneyTemplate<'a> {
    pub fn new(train: &'a Train, trip_id: Option<usize>) -> Self {
        let (previous_trip, next_trip) = if let Some(trip_id) = trip_id {
            let next = train.trips().get(trip_id + 1).map(|t| (trip_id + 1, t));
            if trip_id > 0 {
                (
                    train.trips().get(trip_id - 1).map(|t| (trip_id - 1, t)),
                    next,
                )
            } else {
                (None, next)
            }
        } else {
            (train.trips().iter().enumerate().last(), None)
        };

        let locations = train_journey_points(train, trip_id);
        let current_trip = trip_id.and_then(|t| train.trips().get(t));
        let trip_id = trip_id.unwrap_or_else(|| train.current_trip_id());
        let next_trip_is_current = trip_id + 1 == train.current_trip_id();

        let title = make_journey_title(train, current_trip);

        let base = BaseTemplate {
            title: title.clone().into(),
            accent_colour: get_line_by_code(train.line_code())
                .map(|x| x.html_colour)
                .unwrap_or("black"),
            ..Default::default()
        };

        Self {
            base,
            train,
            locations,
            previous_trip,
            current_trip,
            next_trip,
            next_trip_is_current,
            trip_id,
            title,
        }
    }
    pub fn line_name(&self) -> &str {
        get_line_by_code(self.train.line_code())
            .map(|x| x.name)
            .unwrap_or("???")
    }
}

#[derive(Template)]
#[template(path = "train.html")]
pub struct TrainTemplate<'a> {
    pub base: BaseTemplate<'a>,
    pub train: &'a Train,
    pub history_entries: Vec<TrainHistoryEntry<'a>>,
    pub current_location: Option<TrainHistoryEntry<'a>>,
}

impl<'a> TrainTemplate<'a> {
    pub fn new(train: &'a Train) -> Self {
        let (current, rest) = match train.locations().split_last() {
            Some((current, rest)) => {
                let mut entry = TrainHistoryEntry::from(current);
                entry.is_current = true;
                (Some(entry), rest)
            }
            None => (None, &[] as &[_]),
        };
        let mut history_entries = train
            .events()
            .iter()
            .map(TrainHistoryEntry::from)
            .chain(rest.iter().map(TrainHistoryEntry::from))
            .collect::<Vec<_>>();
        history_entries.sort_by_key(|e| cmp::Reverse(e.timestamp));
        let title = if let Some(n) = train.train_number() {
            format!("#{n:03} to {}", train.destination().text)
        } else {
            format!("Train to {}", train.destination().text)
        };
        Self {
            base: BaseTemplate {
                title: title.into(),
                accent_colour: get_line_by_code(train.line_code())
                    .map(|x| x.html_colour)
                    .unwrap_or("black"),
                ..Default::default()
            },
            train,
            history_entries,
            current_location: current,
        }
    }
    pub fn line_name(&self) -> &str {
        get_line_by_code(self.train.line_code())
            .map(|x| x.name)
            .unwrap_or("???")
    }
}

#[derive(Template)]
#[template(path = "event.html")]
pub struct TrainHistoryEntry<'a> {
    pub timestamp: DateTime<Utc>,
    pub end_ts: Option<DateTime<Utc>>,
    pub event: Option<&'a HistoryEventContent>,
    pub location: Option<&'a LocationKind>,
    pub is_current: bool,
}

impl<'a> From<&'a Location> for TrainHistoryEntry<'a> {
    fn from(loc: &'a Location) -> Self {
        TrainHistoryEntry {
            timestamp: loc.start_ts,
            end_ts: Some(loc.end_ts),
            event: None,
            location: Some(&loc.kind),
            is_current: false,
        }
    }
}

impl<'a> From<&'a HistoryEvent> for TrainHistoryEntry<'a> {
    fn from(event: &'a HistoryEvent) -> Self {
        TrainHistoryEntry {
            timestamp: event.timestamp,
            end_ts: None,
            event: Some(&event.content),
            location: None,
            is_current: false,
        }
    }
}

impl TrainHistoryEntry<'_> {
    pub fn format_duration(&self) -> String {
        let dur = self.end_ts.unwrap_or(self.timestamp).sub(self.timestamp);
        let secs = dur.num_seconds();
        match secs {
            ..=59 => format!("{secs}s"),
            60 => "1m".to_string(),
            61.. => format!("{}m {}s", secs / 60, secs % 60),
        }
    }
    pub fn hhmm_railway(&self) -> String {
        self.timestamp
            .with_timezone(&Europe::London)
            .format("%H%M")
            .to_string()
    }
    pub fn secs_railway(&self) -> Option<&'static str> {
        match self.timestamp.second() {
            0..=14 => None,
            15..=29 => Some("¼"),
            30..=44 => Some("½"),
            45.. => Some("¾"),
        }
    }
    pub fn location_name_and_platform(&self) -> Option<(&str, Option<&str>)> {
        match self.location {
            Some(LocationKind::Station { name, platform }) => Some((name, platform.as_deref())),
            Some(LocationKind::Other { name }) => Some((name, None)),
            _ => None,
        }
    }
}

#[derive(Template)]
#[template(path = "line-basic.html")]
pub struct LineOverviewBasicTemplate<'a> {
    pub base: BaseTemplate<'a>,
    pub line_name: String,
    pub trains_by_destination: Vec<(String, Vec<&'a Train>)>,
}

impl<'a> LineOverviewBasicTemplate<'a> {
    pub fn new(line: LineData, data: &'a TrainDataInner) -> Self {
        let mut trains_by_destination: HashMap<String, Vec<&Train>> = HashMap::new();
        for (_, train) in data.trains.iter() {
            if train.line_code() == line.code {
                trains_by_destination
                    .entry(train.destination().text.clone())
                    .or_default()
                    .push(train);
            }
        }
        let mut trains_by_destination = trains_by_destination
            .into_iter()
            .map(|(dest, mut trains)| {
                trains.sort_by_key(|t| t.train_number().unwrap_or(u16::MAX));
                (dest, trains)
            })
            .collect::<Vec<_>>();
        trains_by_destination.sort_by(|(da, _), (db, _)| da.cmp(db));

        Self {
            base: BaseTemplate {
                title: format!("{} line", line.name).into(),
                accent_colour: line.html_colour,
                ..Default::default()
            },
            line_name: line.name.into(),
            trains_by_destination,
        }
    }
}

#[derive(Template)]
#[template(path = "route-point.html")]
pub struct FilledRoutePointTemplate<'a> {
    pub line_code: &'static str,
    pub line_colour: &'static str,
    pub route_point: FilledRoutePoint<'a>,
    pub train_descriptions: Rc<HashMap<String, String>>,
}

impl<'a> FilledRoutePointTemplate<'a> {
    // this is needed for silly askama reasons
    pub fn route_point_ref(&self) -> &FilledRoutePoint<'a> {
        &self.route_point
    }
    pub fn train_description(&self, id: &str) -> String {
        self.train_descriptions
            .get(id)
            .cloned()
            .unwrap_or_else(|| "<unknown>".into())
    }
}

pub fn template_route_points(
    results: FilledRouteResults,
    line_data: LineData,
) -> Vec<(usize, FilledRoutePointTemplate)> {
    let train_descriptions = Rc::new(results.train_descriptions);
    results
        .points
        .into_iter()
        .map(|x| FilledRoutePointTemplate {
            line_colour: line_data.html_colour,
            line_code: line_data.code,
            route_point: x,
            train_descriptions: Rc::clone(&train_descriptions),
        })
        .enumerate()
        .collect()
}

#[derive(Template)]
#[template(path = "line-model.html")]
pub struct LineOverviewModelTemplate<'a> {
    pub base: BaseTemplate<'a>,
    pub line_name: &'static str,
    pub line_code: &'static str,
    pub groups: &'a [RouteGroup],
    pub current_route: &'a Route,
    pub route_points: Vec<(usize, FilledRoutePointTemplate<'a>)>,
}

impl<'a> LineOverviewModelTemplate<'a> {
    pub fn new<'b: 'a>(
        line: LineData,
        train_data: &'a TrainDataInner,
        route_file: &'b RouteFile,
        route: Option<&str>,
    ) -> Self {
        let current_route = route
            .and_then(|x| route_file.route(x))
            .unwrap_or_else(|| route_file.default_route());
        let results = get_filled_route(current_route, train_data);
        let route_points = template_route_points(results, line);

        Self {
            base: BaseTemplate {
                title: format!("{} line", line.name).into(),
                accent_colour: line.html_colour,
                scroll_snap: true,
                ..Default::default()
            },
            line_name: line.name,
            line_code: line.code,
            groups: &route_file.order,
            current_route,
            route_points,
        }
    }
    pub fn currently_in_group(&self, grp: &'a RouteGroup) -> bool {
        self.current_route
            .group_id
            .as_ref()
            .map(|x| x == &grp.id)
            .unwrap_or(false)
    }
}

#[derive(Template)]
#[template(path = "choose-train.html")]
pub struct ChooseTrainTemplate<'a> {
    pub base: BaseTemplate<'a>,
    pub line_name: &'static str,
    pub line_code: &'static str,
    pub trains: Vec<&'a Train>,
}

impl<'a> ChooseTrainTemplate<'a> {
    pub fn new(train_data: &'a TrainDataInner, payload: &str) -> anyhow::Result<Self> {
        let mut trains = payload
            .split(',')
            .flat_map(|tid| train_data.trains.get(tid))
            .collect::<Vec<_>>();
        trains.sort_by_key(|t| t.train_number().unwrap_or(u16::MAX));
        let line_code = trains
            .first()
            .ok_or_else(|| anyhow!("no trains returned"))?
            .line_code();
        let line =
            get_line_by_code(line_code).ok_or_else(|| anyhow!("invalid line {line_code}"))?;

        Ok(Self {
            base: BaseTemplate {
                title: "Choose a train".into(),
                accent_colour: line.html_colour,
                ..Default::default()
            },
            line_name: line.name,
            line_code: line.code,
            trains,
        })
    }
}

#[derive(Template)]
#[template(path = "error.html")]
pub struct ErrorTemplate<'a> {
    pub title: Cow<'a, str>,
    pub status_code: u16,
    pub text: Option<Cow<'a, str>>,
    pub base: BaseTemplate<'a>,
}
