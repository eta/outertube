//! Computing route data and streaming it to clients live & on demand.

use crate::processor::{HistoryEventContent, Location, ProcessorEvent};
use crate::routes::{Route, RoutePoint};
use crate::storage::{TrainData, TrainDataInner};
use crate::web::lines::LineData;
use crate::web::templates::template_route_points;
use anyhow::Context;
use askama::Template;
use serde::Serialize;
use slog::{error, info, o, trace, warn, Logger};
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, RwLock};
use std::thread;
use std::time::{Duration, Instant};

/// The minimum time after which a `RouteStreamer` shuts down.
pub const ROUTE_STREAMER_EXPIRY: Duration = Duration::from_secs(60 * 5);

pub struct FilledRouteResults<'a> {
    pub points: Vec<FilledRoutePoint<'a>>,
    pub train_descriptions: HashMap<String, String>,
}

#[derive(Serialize)]
pub struct FilledRoutePoint<'a> {
    #[serde(flatten)]
    pub point: &'a RoutePoint,
    pub trains_here: Vec<String>,
    #[serde(skip_serializing)]
    pub is_first: bool,
    #[serde(skip_serializing)]
    pub is_last: bool,
}

pub fn get_filled_route<'a>(route: &'a Route, data: &TrainDataInner) -> FilledRouteResults<'a> {
    // this is necessary in case there's a `Crossreference` before the stations begin
    let stations_only = route
        .points
        .iter()
        .enumerate()
        .filter(|(_, x)| matches!(x, RoutePoint::Station { .. }))
        .collect::<Vec<_>>();
    let points = route
        .points
        .iter()
        .enumerate()
        .map(|(i, point)| {
            let is_first = i == 0 || i == stations_only.first().unwrap().0;
            let is_last = i + 1 == route.points.len() || i == stations_only.last().unwrap().0;
            match point {
                point @ RoutePoint::Station { modelpoints, .. }
                | point @ RoutePoint::Transit { modelpoints, .. } => {
                    // TODO: filter trains by line!
                    let trains_here = data
                        .trains
                        .iter()
                        .filter(|(_, train)| {
                            train
                                .current_model_points()
                                .map(|points| points.iter().any(|p| modelpoints.contains(p)))
                                .unwrap_or(false)
                        })
                        .map(|(tid, _)| tid.clone())
                        .collect::<Vec<_>>();
                    // TODO: sort the trains_here by progress
                    FilledRoutePoint {
                        point,
                        trains_here,
                        is_first,
                        is_last,
                    }
                }
                point => FilledRoutePoint {
                    point,
                    trains_here: vec![],
                    is_first,
                    is_last,
                },
            }
        })
        .collect::<Vec<_>>();
    let mut train_descriptions = HashMap::new();
    for train in points.iter().flat_map(|p| p.trains_here.iter()) {
        train_descriptions.insert(
            train.to_owned(),
            data.trains
                .get(train)
                .unwrap()
                .current_description()
                .to_string(),
        );
    }
    FilledRouteResults {
        points,
        train_descriptions,
    }
}

#[derive(Serialize)]
struct RouteUpdate<'a> {
    id: usize,
    html: &'a str,
}

#[derive(Serialize)]
struct InitialStateUpdate {
    all_html: Vec<String>,
}

pub struct RouteStreamer {
    line_data: LineData,
    log: Logger,
    route: &'static Route,
    train_data: Arc<TrainData>,
    rx: async_broadcast::Receiver<ProcessorEvent>,
    tx: async_broadcast::Sender<String>,
    templated_route_points: Arc<RwLock<Vec<String>>>,
    active_train_ids: HashSet<String>,
    model_points: HashSet<String>,
    last_active: Instant,
}

#[derive(Clone)]
pub struct RouteStreamerHandle {
    templated_route_points: Arc<RwLock<Vec<String>>>,
    rx: async_broadcast::Receiver<String>,
}

#[derive(Clone)]
pub struct InactiveRouteStreamerHandle {
    templated_route_points: Arc<RwLock<Vec<String>>>,
    rx: async_broadcast::InactiveReceiver<String>,
}

impl InactiveRouteStreamerHandle {
    /// If the backing route streamer is still active, reactivate this handle.
    pub fn activate(self) -> Option<RouteStreamerHandle> {
        if self.rx.is_closed() {
            None
        } else {
            Some(RouteStreamerHandle {
                templated_route_points: self.templated_route_points,
                rx: self.rx.activate(),
            })
        }
    }
}

impl RouteStreamerHandle {
    /// Make a deactivated version of the handle for others to use later.
    pub fn deactivate(self) -> InactiveRouteStreamerHandle {
        InactiveRouteStreamerHandle {
            templated_route_points: self.templated_route_points,
            rx: self.rx.deactivate(),
        }
    }

    /// Serialize an update message with the initial state of every route point.
    pub fn serialized_initial_state(&self) -> anyhow::Result<String> {
        let state = self.templated_route_points.read().unwrap();
        serde_json::to_string(&InitialStateUpdate {
            all_html: state.clone(),
        })
        .context("failed to serialize initial route state")
    }
    /// Get the next update message to send to the client.
    pub async fn next_update(&mut self) -> anyhow::Result<String> {
        Ok(self.rx.recv_direct().await?)
    }
}

impl RouteStreamer {
    /// Start streaming route updates for the provided line and route.
    ///
    /// This returns a channel that provides JSON-serialized template fragments to send to clients.
    pub fn start(
        line_data: LineData,
        route: &'static Route,
        train_data: Arc<TrainData>,
        log: &Logger,
    ) -> anyhow::Result<RouteStreamerHandle> {
        let (mut tx, rx) = async_broadcast::broadcast::<String>(1024);
        let templated_route_points = Arc::new(RwLock::new(vec![]));

        // Don't wait for there to be receivers; just scream into the void.
        tx.set_await_active(false);
        // Don't let slower receivers bottleneck us.
        tx.set_overflow(true);

        let model_points = route
            .points
            .iter()
            .flat_map(|p| match p {
                RoutePoint::Station { modelpoints, .. } => Some(modelpoints),
                RoutePoint::Transit { modelpoints, .. } => Some(modelpoints),
                _ => None,
            })
            .flatten()
            .map(|x| x.name.clone())
            .collect::<HashSet<_>>();

        let processor_rx = train_data.events_rx.activate_cloned();
        let mut streamer = Self {
            line_data,
            log: log.new(o!("line" => line_data.code, "route" => &route.id)),
            route,
            train_data,
            rx: processor_rx,
            tx,
            templated_route_points: templated_route_points.clone(),
            active_train_ids: Default::default(),
            model_points,
            last_active: Instant::now(),
        };

        streamer.get_updates().context("initial update failed")?;

        thread::spawn(move || {
            smol::block_on(streamer.run());
        });

        Ok(RouteStreamerHandle {
            templated_route_points,
            rx,
        })
    }
    async fn run(mut self) {
        info!(self.log, "starting route streamer");
        while let Ok(event) = self.rx.recv_direct().await {
            let should_redraw = match &event {
                // We have to assume every new train on our line could be on our route.
                ProcessorEvent::New { line_code, .. } => line_code == self.line_data.code,
                ProcessorEvent::Update {
                    line_code,
                    train_id,
                    event,
                } => {
                    // Ignore updates that aren't for our line at all.
                    if line_code != self.line_data.code {
                        false
                    } else {
                        match &event.content {
                            // If one of our trains changes description or model point (but not
                            // location), that requires a redraw.
                            //
                            // Well... technically another train could have its model point
                            // updated this way, but that should really be a new location, so we can
                            // blame the processor :p
                            HistoryEventContent::Transit {
                                description,
                                model_point,
                                ..
                            } => {
                                self.active_train_ids.contains(train_id)
                                    && (description.is_some() || model_point.is_some())
                            }
                            // If a train enters a new location covered by us, that requires a redraw.
                            HistoryEventContent::NewLocation {
                                new_location: Location { model_point, .. },
                                ..
                            } => self.model_points.intersection(model_point).next().is_some(),
                            _ => false,
                        }
                    }
                }
                // Archives and rescues have to touch one of our active trains to redraw.
                ProcessorEvent::Archive { train_id } => self.active_train_ids.contains(train_id),
                ProcessorEvent::Rescue {
                    train_id,
                    target_id,
                } => {
                    self.active_train_ids.contains(train_id)
                        || self.active_train_ids.contains(target_id)
                }
            };
            if should_redraw {
                match self.get_updates() {
                    Ok(updates) => {
                        trace!(self.log, "got {} updates from {event:?}", updates.len());

                        crate::metrics::ROUTE_STREAMER_UPDATE_COUNTER
                            .with_label_values(&[self.line_data.code, &self.route.id])
                            .inc_by(updates.len() as u64);

                        crate::metrics::ROUTE_STREAMER_COUNT
                            .with_label_values(&[self.line_data.code, &self.route.id])
                            .set(self.tx.receiver_count() as f64);

                        for update in updates {
                            if self.tx.broadcast_direct(update).await.is_err() {
                                if self.tx.is_closed() {
                                    warn!(self.log, "route streamer channel closed");
                                    return;
                                }
                                if Instant::now()
                                    .checked_duration_since(self.last_active)
                                    .unwrap_or_default()
                                    > ROUTE_STREAMER_EXPIRY
                                {
                                    info!(
                                        self.log,
                                        "route streamer shutting down due to inactivity"
                                    );
                                    return;
                                }
                            } else {
                                self.last_active = Instant::now();
                            }
                        }
                    }
                    Err(e) => {
                        error!(self.log, "updating route points failed: {e}");
                        return;
                    }
                }
            }
        }
        info!(
            self.log,
            "route streamer shutting down due to channel error"
        );
    }
    fn get_updates(&mut self) -> anyhow::Result<Vec<String>> {
        let data = self.train_data.inner.read().unwrap();

        let filled_route_results = get_filled_route(self.route, &data);
        drop(data);

        self.active_train_ids.clear();
        self.active_train_ids
            .extend(filled_route_results.train_descriptions.keys().cloned());

        let new_points_templated = template_route_points(filled_route_results, self.line_data)
            .into_iter()
            .map(|(_, template)| template.render())
            .collect::<Result<Vec<_>, _>>()
            .context("route templating failed")?;

        let mut templated_route_points = self.templated_route_points.write().unwrap();

        // on first run, don't generate anything
        let updates = if templated_route_points.is_empty() {
            vec![]
        } else {
            templated_route_points
                .iter()
                .zip(new_points_templated.iter())
                .enumerate()
                .filter(|(_, (old_point, new_point))| old_point != new_point)
                .map(|(id, (_old_point, new_point))| {
                    serde_json::to_string(&RouteUpdate {
                        id,
                        html: new_point,
                    })
                })
                .collect::<Result<Vec<_>, _>>()
                .context("serializing updates to json failed")?
        };

        *templated_route_points = new_points_templated;

        Ok(updates)
    }
}
