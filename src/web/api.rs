//! Serving trains and other things from the API.

use crate::routes::RouteFile;
use crate::web::live_routes::get_filled_route;
use crate::web::streaming::JsonlStreamer;
use crate::web::State;
use anyhow::{anyhow, Context};
use prometheus::TextEncoder;
use serde::Serialize;
use slog::info;
use tide::http::mime;
use tide::{Body, Request, Response};

fn json(thing: &impl Serialize) -> tide::Result {
    let ser = serde_json::to_string(thing).context("failed to serialize")?;
    Ok(Response::builder(200)
        .body(ser)
        .content_type(mime::JSON)
        .build())
}

pub(crate) async fn metrics(_: Request<State>) -> tide::Result {
    let mut ret = String::new();
    let encoder = TextEncoder::new();
    encoder.encode_utf8(&prometheus::gather(), &mut ret)?;
    Ok(ret.into())
}

pub(crate) async fn trains(req: Request<State>) -> tide::Result {
    let data = req.state().train_data();
    json(&data.trains)
}

pub(crate) async fn train(req: Request<State>) -> tide::Result {
    let tid = req.param("tid")?;
    let data = req.state().train_data();
    if let Some(train) = data.trains.get(tid) {
        json(&train)
    } else {
        Err(tide::Error::new(
            404,
            anyhow!("could not find train {}", tid),
        ))
    }
}

pub(crate) async fn empty_route(req: Request<State>) -> tide::Result {
    let line = req.param("line")?;
    let route = req.param("route")?;
    if let Some(route) = RouteFile::for_line(line).and_then(|x| x.route(route)) {
        json(&route)
    } else {
        Err(tide::Error::new(
            404,
            anyhow!("could not find route {route} on line {line}"),
        ))
    }
}

pub(crate) async fn filled_route(req: Request<State>) -> tide::Result {
    let line = req.param("line")?;
    let route = req.param("route")?;
    let data = req.state().train_data();
    if let Some(route) = RouteFile::for_line(line).and_then(|x| x.route(route)) {
        json(&get_filled_route(route, &data).points)
    } else {
        Err(tide::Error::new(
            404,
            anyhow!("could not find route {route} on line {line}"),
        ))
    }
}

pub(crate) async fn firehose(req: Request<State>) -> tide::Result {
    let data = req.state().train_data.events_rx.activate_cloned();
    info!(
        req.state().log,
        "new firehose connection";
        "remote" => req.remote().unwrap_or("???")
    );
    crate::metrics::FIREHOSE_CONNECTIONS_COUNT.inc();
    let log = req.state().log.clone();
    let remote = req.remote().unwrap_or("???").to_owned();
    let on_close = move || {
        info!(log, "firehose connection closed"; "remote" => remote);
        crate::metrics::FIREHOSE_CONNECTIONS_COUNT.dec();
    };
    Ok(Response::builder(200)
        .body(Body::from_reader(JsonlStreamer::new(data, on_close), None))
        .build())
}
