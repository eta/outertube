//! Tube line themes and such!

#[derive(Copy, Clone, Debug)]
pub struct LineData {
    pub name: &'static str,
    pub code: &'static str,
    pub html_colour: &'static str,
}

pub static BAKERLOO: LineData = LineData {
    name: "Bakerloo",
    code: "B",
    html_colour: "rgb(178, 99, 0)",
};

pub static CENTRAL: LineData = LineData {
    name: "Central",
    code: "C",
    html_colour: "rgb(220, 36, 31)",
};

pub static CIRCLE: LineData = LineData {
    name: "Circle",
    code: "H",
    html_colour: "rgb(255, 200, 10)",
};

pub static DISTRICT: LineData = LineData {
    name: "District",
    code: "D",
    html_colour: "rgb(0, 125, 50)",
};

pub static JUBILEE: LineData = LineData {
    name: "Jubilee",
    code: "J",
    html_colour: "rgb(131, 141, 147)",
};

pub static METROPOLITAN: LineData = LineData {
    name: "Metropolitan",
    code: "M",
    html_colour: "rgb(155, 0, 88)",
};

pub static NORTHERN: LineData = LineData {
    name: "Northern",
    code: "N",
    html_colour: "rgb(0, 0, 0)",
};

pub static PICCADILLY: LineData = LineData {
    name: "Piccadilly",
    code: "P",
    html_colour: "rgb(0, 25, 168)",
};

pub static VICTORIA: LineData = LineData {
    name: "Victoria",
    code: "V",
    html_colour: "rgb(3, 155, 229)",
};

pub static WATERLOO_AND_CITY: LineData = LineData {
    name: "Waterloo & City",
    code: "W",
    html_colour: "rgb(118, 208, 189)",
};

pub static ALL_LINES: &[LineData] = &[
    BAKERLOO,
    CENTRAL,
    CIRCLE,
    DISTRICT,
    JUBILEE,
    METROPOLITAN,
    NORTHERN,
    PICCADILLY,
    VICTORIA,
    WATERLOO_AND_CITY,
];

pub fn get_line_by_code(code: &str) -> Option<LineData> {
    Some(match code {
        "B" => BAKERLOO,
        "C" => CENTRAL,
        "H" => CIRCLE,
        "D" => DISTRICT,
        "J" => JUBILEE,
        "M" => METROPOLITAN,
        "N" => NORTHERN,
        "P" => PICCADILLY,
        "V" => VICTORIA,
        "W" => WATERLOO_AND_CITY,
        _ => return None,
    })
}
