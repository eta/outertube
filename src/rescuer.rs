//! Joining together trains which are likely the same but have different identifiers.

use crate::metrics::TRAINS_COUNT;
use crate::model::{Modelpoint, NetworkModel};
use crate::processor::{ProcessorEvent, Train};
use crate::rescuer;
use crate::storage::{TrainData, TrainDataInner};
use anyhow::anyhow;
use async_broadcast::Sender;
use chrono::Utc;
use chrono_tz::Europe;
use slog::{debug, error, info, o, Logger};
use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashMap};
use std::sync::Arc;
use std::time::{Duration, Instant};
use std::{mem, thread};

const RESCUE_MAX_DEPTH: usize = 2;
pub const ARCHIVER_GRACE_PERIOD: Duration = Duration::from_secs(25);

/// A potential rescue candidate.
#[derive(Debug, PartialEq, Eq)]
struct RescueCandidate {
    /// The train id being rescued.
    source: String,
    /// The new train id we think `source` is a continuation of.
    target: String,
    /// The time difference between `source`'s last update and `dest`'s first update.
    time_difference_sec: i64,
    /// Do the two trains share a leading car identifier?
    same_leading_car: bool,
    /// Do the two trains share a track code?
    same_track_code: bool,
    /// What is the route distance between the two trains on the toposort model?
    /// (this is usually two or three, or `u32::MAX` if we don't have one)
    hop_difference: usize,
}

impl Ord for RescueCandidate {
    fn cmp(&self, other: &Self) -> Ordering {
        // Sharing a leading car is most important.
        if self.same_leading_car && !other.same_leading_car {
            return Ordering::Greater;
        }
        // Sharing a track code is also important.
        if self.same_track_code && !other.same_track_code {
            return Ordering::Greater;
        }
        // If this train has a smaller time difference, it's more important.
        let time_difference = self
            .time_difference_sec
            .cmp(&other.time_difference_sec)
            .reverse();
        if !time_difference.is_eq() {
            return time_difference;
        }
        // If the two time differences are equal, return the train with the lowest hop difference.
        self.hop_difference.cmp(&other.hop_difference).reverse()
    }
}

impl PartialOrd for RescueCandidate {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn compute_routes_between(
    source: &Train,
    target: &Train,
    max_depth: usize,
) -> Option<Vec<Vec<&'static Modelpoint>>> {
    let model = NetworkModel::for_line(source.line_code()).unwrap();
    let source_points = source
        .current_location()?
        .model_point
        .iter()
        .filter_map(|x| model.get(x))
        .collect::<Vec<_>>();
    let target_points = target
        .first_location()?
        .model_point
        .iter()
        .filter_map(|x| model.get(x))
        .collect::<Vec<_>>();

    let mut ret = vec![];
    for spt in source_points.iter() {
        for tpt in target_points.iter() {
            if let Some(route) = model.route_between(spt, tpt, max_depth) {
                ret.push(route);
            }
        }
    }
    if !ret.is_empty() {
        Some(ret)
    } else {
        None
    }
}

fn compute_rescue_candidates(
    source: &Train,
    targets: &[&Train],
    log: &Logger,
) -> Vec<RescueCandidate> {
    let mut ret = vec![];
    for target in targets {
        // They can't be the same
        if source.unique_id() == target.unique_id() {
            continue;
        }
        // They have to be on the same line
        if source.line_code() != target.line_code() {
            continue;
        }
        // If they both have a leading car number, the numbers can't be different
        if source.leading_car_no().is_some()
            && target.leading_car_no().is_some()
            && source.leading_car_no() != target.leading_car_no()
        {
            continue;
        }
        // The target must be newer than the source
        if source.last_updated() > target.first_updated() {
            continue;
        }
        let time_difference_sec = (target.first_updated() - source.last_updated()).num_seconds();
        let same_leading_car = source.leading_car_no().is_some()
            && target.leading_car_no().is_some()
            && source.leading_car_no() == target.leading_car_no();
        if same_leading_car {
            debug!(
                log,
                "rescuer: {} and {} share leading car {}",
                source.unique_id(),
                target.unique_id(),
                source.leading_car_no().unwrap()
            );
        }
        let same_track_code = source.current_track_code().is_some()
            && target.first_track_code().is_some()
            && source.current_track_code() == target.first_track_code();
        if same_track_code {
            debug!(
                log,
                "rescuer: {} and {} share track code {}",
                source.unique_id(),
                target.unique_id(),
                source.current_track_code().unwrap()
            );
        }
        if let Some(routes) = compute_routes_between(source, target, RESCUE_MAX_DEPTH) {
            for route in routes {
                debug!(
                    log,
                    "rescuer: {} and {} have route {:?}",
                    source.unique_id(),
                    target.unique_id(),
                    route.iter().map(|x| &x.name).collect::<Vec<_>>()
                );
                ret.push(RescueCandidate {
                    source: source.unique_id().to_owned(),
                    target: target.unique_id().to_owned(),
                    time_difference_sec,
                    same_leading_car,
                    same_track_code,
                    hop_difference: route.len(),
                })
            }
        } else if same_leading_car || same_track_code {
            ret.push(RescueCandidate {
                source: source.unique_id().to_owned(),
                target: target.unique_id().to_owned(),
                time_difference_sec,
                same_leading_car,
                same_track_code,
                hop_difference: usize::MAX,
            })
        }
    }
    ret
}

fn perform_rescue(
    train_data: &mut TrainDataInner,
    candidate: RescueCandidate,
    events_tx: &Sender<ProcessorEvent>,
    log: &Logger,
) -> anyhow::Result<()> {
    let log = log.new(
        o!("train_id" => candidate.source.clone(), "rescue_target" => candidate.target.clone()),
    );
    // This is required so that we don't drop the `target` on the floor in remove() below.
    // (It's also not really an error, so don't treat it as one.)
    if !train_data.trains.contains_key(&candidate.source) {
        info!(log, "can't do rescue, since source is gone");
        return Ok(());
    }
    let target = train_data
        .trains
        .remove(&candidate.target)
        .ok_or_else(|| anyhow!("rescue target {} disappeared", candidate.target))?;
    let source = train_data
        .trains
        .get_mut(&candidate.source)
        .ok_or_else(|| anyhow!("rescue source {} disappeared", candidate.source))?;
    info!(
        log,
        "rescue: {} train {} => {} train {} (lc {}, tc {}, hops {})",
        source.line_code(),
        source.trackernet_key(),
        target.line_code(),
        target.trackernet_key(),
        candidate.same_leading_car,
        candidate.same_track_code,
        if candidate.hop_difference == usize::MAX {
            "none".to_string()
        } else {
            candidate.hop_difference.to_string()
        }
    );
    assert!(train_data.key_map.remove(&source.identity_key()).is_some());
    let target_identity = target.identity_key();
    let start = Instant::now();
    let target_observations = train_data
        .raw_observations
        .remove(target.unique_id())
        .unwrap_or_default();
    let mut source_observations = train_data.raw_observations.get_mut(source.unique_id());
    let obs_count = target_observations.len();
    for (time, obs) in target_observations {
        if let Some(ref mut srco) = source_observations {
            srco.push((time, obs.clone()));
        }
        source.apply_observation(obs, time.with_timezone(&Europe::London), events_tx, &log);
    }
    let length = Instant::now() - start;
    info!(
        log,
        "processed rescue of {} observations in {}ms",
        obs_count,
        length.as_millis()
    );
    let _ = events_tx.broadcast_blocking(ProcessorEvent::Rescue {
        train_id: candidate.source.clone(),
        target_id: candidate.target.clone(),
    });
    assert_eq!(source.identity_key(), target_identity);
    assert!(train_data
        .key_map
        .insert(source.identity_key(), source.unique_id().to_owned())
        .is_some());
    Ok(())
}

fn rescuer(train_data: &TrainData, events_tx: &Sender<ProcessorEvent>, log: &Logger) {
    let td_read = train_data.inner.read().unwrap();
    let now = Utc::now();
    let stale_trains = td_read
        .trains
        .iter()
        .filter(|(_, v)| v.is_stale(now))
        .map(|(_, v)| v);
    let rescue_targets = td_read
        .trains
        .iter()
        .filter(|(_, v)| v.is_rescue_target(now))
        .map(|(_, v)| v)
        .collect::<Vec<_>>();
    // A map of rescue candidates indexed by destination, so the 'most important' one for each
    // destination gets used.
    let mut candidate_map: HashMap<String, BinaryHeap<RescueCandidate>> = HashMap::new();

    for train in stale_trains {
        for candidate in compute_rescue_candidates(train, &rescue_targets, log) {
            candidate_map
                .entry(candidate.target.clone())
                .or_default()
                .push(candidate);
        }
    }

    if candidate_map.is_empty() {
        return;
    }

    mem::drop(td_read);
    let mut td_write = train_data.inner.write().unwrap();

    for (_target, mut heap) in candidate_map {
        if let Some(top) = heap.pop() {
            if let Err(e) = perform_rescue(&mut td_write, top, events_tx, log) {
                error!(log, "rescue failed: {e}");
            }
        }
    }
}

fn archiver(train_data: &TrainData, events_tx: &Sender<ProcessorEvent>, log: &Logger) {
    let mut inner = train_data.inner.write().unwrap();
    let inner = &mut *inner;
    let now = Utc::now();
    let mut dead_keys = vec![];
    inner.trains.retain(|_, train| {
        if train.is_expired(now) {
            info!(log, "archiving train"; "train_id" => train.unique_id());
            inner.raw_observations.remove(train.unique_id());
            let _ = events_tx.broadcast_blocking(ProcessorEvent::Archive {
                train_id: train.unique_id().to_owned(),
            });
            dead_keys.push((
                train.line_code().to_owned(),
                train.trackernet_key().to_owned(),
            ));
            false
        } else {
            true
        }
    });
    for key in dead_keys {
        assert!(inner.key_map.remove(&key).is_some());
    }
    TRAINS_COUNT.set(inner.trains.len() as f64);
}

pub fn start_rescuer_and_archiver(
    train_data: Arc<TrainData>,
    events_tx: Sender<ProcessorEvent>,
    log: Logger,
) {
    let app_start = Instant::now();
    thread::spawn(move || loop {
        std::thread::sleep(Duration::from_secs(1));
        // Give trains a chance to get scraped after application start before archiving them.
        if Instant::now() - app_start < rescuer::ARCHIVER_GRACE_PERIOD {
            continue;
        }
        rescuer(&train_data, &events_tx, &log);
        archiver(&train_data, &events_tx, &log);
    });
}
