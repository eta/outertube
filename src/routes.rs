//! Tracking trains along routes intended for human presentation.
//!
//! Our goal here is to enable the drawing of maps showing all the trains along a route - for
//! example, the Jubilee line from Stanmore to Stratford, or the Northern line from Morden to
//! High Barnet via Bank.

use crate::model::{Modelpoint, NetworkModel};
use anyhow::{anyhow, bail, Context};
use lazy_static::lazy_static;
use petgraph::graph::{DefaultIx, NodeIndex};
use petgraph::Graph;
use serde::de::Error;
use serde::{Deserialize, Deserializer, Serialize};
use std::collections::{HashMap, HashSet};
use std::ops::Deref;
use std::str::FromStr;

#[derive(Debug, Clone)]
pub struct RouteFile {
    pub order: Vec<RouteGroup>,
    pub routes: HashMap<String, Route>,
    pub groups: HashMap<String, RouteGroup>,
}

impl RouteFile {
    /// Get the route file for the given `line` code.
    pub fn for_line(line: &str) -> Option<&'static Self> {
        static ROUTE_B_TOML: &str = include_str!("../routes/B.toml");
        static ROUTE_C_TOML: &str = include_str!("../routes/C.toml");
        static ROUTE_J_TOML: &str = include_str!("../routes/J.toml");
        static ROUTE_N_TOML: &str = include_str!("../routes/N.toml");
        static ROUTE_P_TOML: &str = include_str!("../routes/P.toml");
        static ROUTE_V_TOML: &str = include_str!("../routes/V.toml");
        static ROUTE_W_TOML: &str = include_str!("../routes/W.toml");

        lazy_static! {
            static ref ROUTE_B: RouteFile = ROUTE_B_TOML.parse().unwrap();
            static ref ROUTE_C: RouteFile = ROUTE_C_TOML.parse().unwrap();
            static ref ROUTE_J: RouteFile = ROUTE_J_TOML.parse().unwrap();
            static ref ROUTE_N: RouteFile = ROUTE_N_TOML.parse().unwrap();
            static ref ROUTE_P: RouteFile = ROUTE_P_TOML.parse().unwrap();
            static ref ROUTE_V: RouteFile = ROUTE_V_TOML.parse().unwrap();
            static ref ROUTE_W: RouteFile = ROUTE_W_TOML.parse().unwrap();
        }

        match line {
            "B" => Some(ROUTE_B.deref()),
            "C" => Some(ROUTE_C.deref()),
            "J" => Some(ROUTE_J.deref()),
            "N" => Some(ROUTE_N.deref()),
            "P" => Some(ROUTE_P.deref()),
            "V" => Some(ROUTE_V.deref()),
            "W" => Some(ROUTE_W.deref()),
            _ => None,
        }
    }
    pub fn default_route(&self) -> &Route {
        let first_group = self.order.first().unwrap();
        self.route(&first_group.default).unwrap()
    }
    pub fn route(&self, route_id: &str) -> Option<&Route> {
        self.routes.get(route_id)
    }
}

impl FromStr for RouteFile {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let raw: RawRouteFile =
            toml::from_str(s).context("failed to parse route definition toml")?;
        #[cfg(test)]
        eprintln!("processing routes for {}", raw.line_code);
        let model = NetworkModel::for_line(&raw.line_code);
        let mut ret = RouteFile {
            routes: Default::default(),
            groups: Default::default(),
            order: vec![],
        };
        for route in raw.routes {
            let id = route.id.clone();
            if ret.routes.insert(route.id.clone(), route).is_some() {
                bail!("two routes share the id {id}");
            }
        }
        if raw.groups.is_empty() {
            for (id, route) in ret.routes.clone() {
                ret.groups.insert(
                    id.clone(),
                    RouteGroup {
                        id: id.clone(),
                        descr: route.descr.clone().ok_or_else(|| {
                            anyhow!("route {id} lacks a descr (and there are no groups)")
                        })?,
                        hint: route.hint.clone(),
                        default: id.clone(),
                    },
                );
                ret.routes.get_mut(&id).unwrap().group_id = Some(id.clone());
            }
        } else {
            for group in raw.groups {
                let id = group.id.clone();
                if !ret.routes.contains_key(&group.default) {
                    bail!("group {id} has invalid default {}", group.default);
                }
                if ret.groups.insert(group.id.clone(), group).is_some() {
                    bail!("two groups share the id {id}");
                }
            }
        }
        ret.order = raw
            .order
            .into_iter()
            .map(|id| {
                ret.groups
                    .get(&id)
                    .cloned()
                    .ok_or_else(|| anyhow!("invalid reference to {id} in order"))
            })
            .collect::<Result<Vec<_>, _>>()?;
        // We want to 'denormalize' the routes so none of them contain references to other routes.
        // But routes can have dependencies on other routes, so we need to do it in the correct
        // order!
        // Build a graph of what routes depend on what other routes, and then get a topological
        // ordering; we expand the routes in the reverse of that order.
        // (note: this is probably way overkill)
        let mut deps = Graph::<String, ()>::new();
        let mut indices: HashMap<String, NodeIndex<DefaultIx>> = HashMap::new();
        for (id, route) in ret.routes.iter() {
            let id = *indices
                .entry(id.to_owned())
                .or_insert_with(|| deps.add_node(id.to_owned()));
            for point in route.points.iter() {
                if let RoutePoint::Route { name } = point {
                    let name = *indices
                        .entry(name.to_owned())
                        .or_insert_with(|| deps.add_node(name.to_owned()));
                    deps.add_edge(id, name, ());
                }
            }
        }
        let toposorted = match petgraph::algo::toposort(&deps, None) {
            Ok(res) => res
                .into_iter()
                .flat_map(|x| deps.remove_node(x))
                .collect::<Vec<String>>(),
            Err(err) => {
                bail!(
                    "cycle detected in route dependencies (one member is {})",
                    deps.remove_node(err.node_id()).unwrap()
                );
            }
        };
        // Process each route:
        for route_id in toposorted.into_iter().rev() {
            #[cfg(test)]
            eprintln!("processing {route_id}");
            if !ret.routes.contains_key(&route_id) {
                bail!("invalid route reference to {route_id}");
            }
            // Denormalize all the route references into just the stations (and xrefs)
            let mut denormalized_points = vec![];
            for point in ret.routes.get(&route_id).unwrap().points.clone() {
                match point {
                    RoutePoint::Route { name } => {
                        let other_route = ret.routes.get(&name).ok_or_else(|| {
                            anyhow!("invalid route reference to {name} from route {route_id}")
                        })?;
                        #[cfg(test)]
                        eprintln!("  adding {name}");
                        denormalized_points.extend(other_route.points.iter().cloned());
                    }
                    RoutePoint::Crossreference {
                        ref route, ref id, ..
                    } => {
                        if !ret.routes.contains_key(route) {
                            bail!("{route_id} xref {id} contains invalid reference to {route}");
                        }
                        denormalized_points.push(point);
                    }
                    other => denormalized_points.push(other),
                }
            }
            // Add the modelpoints to the stations, if we have a model
            if let Some(model) = model {
                for point in denormalized_points.iter_mut() {
                    if let RoutePoint::Station {
                        name,
                        platforms,
                        modelpoints,
                    } = point
                    {
                        for pfm in platforms {
                            modelpoints
                                .extend(model.get_by_station(name.clone(), Some(pfm.clone())));
                        }
                    }
                }
            }
            // We're done!
            ret.routes.get_mut(&route_id).unwrap().points = denormalized_points;
        }
        for (_id, route) in ret.routes.iter_mut() {
            #[cfg(test)]
            eprintln!("adding transit points to {_id}");
            // Add the `Transit` points. We do this by getting pairs of stations with their indices,
            // and making a list of insertions to do, which we then do in reverse so the indices
            // don't break
            let just_the_stations = route
                .points
                .iter()
                .enumerate()
                .filter(|(_, pt)| matches!(pt, RoutePoint::Station { .. }))
                .collect::<Vec<_>>();
            let mut insertions = vec![];
            for station_pair in just_the_stations.windows(2) {
                let &[(
                    _,
                    RoutePoint::Station {
                        name: _first_name,
                        modelpoints: first_points,
                        ..
                    },
                ), (
                    insert_at,
                    RoutePoint::Station {
                        name: _second_name,
                        modelpoints: second_points,
                        ..
                    },
                )] = station_pair
                else {
                    unreachable!()
                };
                #[cfg(test)]
                eprintln!(
                    "  pair {} -> {}, insert at {}",
                    _first_name, _second_name, insert_at
                );
                // figure out the modelpoints for this transit point
                let mut transit_points: HashSet<&'static Modelpoint> = HashSet::new();
                for from in first_points {
                    for to in second_points {
                        if let Some(route) = model.and_then(|x| x.route_between(from, to, 3)) {
                            if route.len() > 2 {
                                // lop off the start and end points
                                transit_points.extend(&route[1..(route.len() - 1)]);
                            }
                        }
                    }
                }
                insertions.push((
                    insert_at,
                    RoutePoint::Transit {
                        modelpoints: transit_points,
                    },
                ));
            }
            // do the insertions
            for (insert_at, point) in insertions.into_iter().rev() {
                route.points.insert(insert_at, point);
            }
        }
        Ok(ret)
    }
}

#[derive(Deserialize, Debug, Clone)]
struct RawRouteFile {
    line_code: String,
    #[serde(rename = "route")]
    routes: Vec<Route>,
    #[serde(rename = "group")]
    #[serde(default)]
    groups: Vec<RouteGroup>,
    order: Vec<String>,
}

/// A group of `Route`s that are all 'going the same way'.
///
/// We use these to avoid exposing all the permutations of e.g. northbound routes on the Northern
/// Line to the user directly; instead, we group them all in the UI as "northbound", and provide
/// crossreference buttons in order to tweak the permutations.
#[derive(Deserialize, Debug, Clone)]
pub struct RouteGroup {
    pub id: String,
    pub descr: String,
    #[serde(default)]
    pub hint: Option<String>,
    pub default: String,
}

/// The items that make up a `Route` in the route definition files.
#[derive(Serialize, Debug, Clone)]
#[serde(tag = "kind")]
pub enum RoutePoint {
    /// A station.
    Station {
        name: String,
        platforms: Vec<String>,
        /// The modelpoints associated with this station.
        modelpoints: HashSet<&'static Modelpoint>,
    },
    /// The space between two stations.
    ///
    /// This gets generated at route parsing time, and isn't present in the route definitions.
    Transit {
        /// The modelpoints associated with this intermediate bit.
        modelpoints: HashSet<&'static Modelpoint>,
    },
    /// A link to another route that the user can click.
    Crossreference {
        /// The route id this leads to.
        route: String,
        /// The identifier of this `Crossreference`.
        id: String,
        /// The identifier of the remote `Crossreference` this link should go to, if not the same
        /// as this one's `id`.
        to_id: Option<String>,
        /// The text to display to the user.
        description: String,
    },
    /// An embedding of another `Route` in this one.
    Route { name: String },
}

/// Deserialize a `RoutePoint` from a simplified format. Points can be either:
///
/// - an array of ["station name", 1, 2] for a station
/// - a route as { route = "route_name" }
/// - a crossreference as { xref = "route_name", id = "id", description = "zone" }
impl<'de> Deserialize<'de> for RoutePoint {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(untagged)]
        enum StringOrInteger {
            Integer(u16),
            String(String),
        }

        impl StringOrInteger {
            fn string(self) -> Option<String> {
                if let Self::String(i) = self {
                    Some(i)
                } else {
                    None
                }
            }
        }

        #[derive(Deserialize)]
        #[serde(untagged)]
        enum RoutePointRepr {
            Station(Vec<StringOrInteger>),
            Route {
                route: String,
            },
            Crossreference {
                xref: String,
                id: String,
                #[serde(default)]
                to_id: Option<String>,
                descr: String,
            },
        }

        let repr = RoutePointRepr::deserialize(deserializer)?;

        Ok(match repr {
            RoutePointRepr::Station(data) => {
                let mut data = data.into_iter();
                let name = data
                    .next()
                    .ok_or_else(|| Error::custom("route point is an empty array"))?
                    .string()
                    .ok_or_else(|| Error::custom("station name must be a string"))?;
                let platforms = data
                    .map(|x| match x {
                        StringOrInteger::Integer(i) => i.to_string(),
                        StringOrInteger::String(s) => s,
                    })
                    .collect::<Vec<_>>();

                RoutePoint::Station {
                    name,
                    platforms,
                    modelpoints: Default::default(),
                }
            }
            RoutePointRepr::Route { route } => RoutePoint::Route { name: route },
            RoutePointRepr::Crossreference {
                xref,
                id,
                to_id,
                descr,
            } => RoutePoint::Crossreference {
                route: xref,
                id,
                to_id,
                description: descr,
            },
        })
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Route {
    pub id: String,
    pub group_id: Option<String>,
    pub descr: Option<String>,
    pub hint: Option<String>,
    pub points: Vec<RoutePoint>,
}

#[cfg(test)]
mod test {
    use crate::routes::{RouteFile, RoutePoint};

    #[test]
    fn routes_parse_and_validate() {
        for line in &["B", "C", "J", "N", "P", "V", "W"] {
            eprintln!("validating routes for {line}");
            let route_file = RouteFile::for_line(line).unwrap();
            for (id, route) in route_file.routes.iter() {
                eprintln!("validating {id}");
                let mut expect_station = true;
                for point in route.points.iter() {
                    match point {
                        RoutePoint::Station { name, .. } => {
                            if expect_station {
                                eprintln!("  station {name}");
                                expect_station = false;
                            } else {
                                panic!("unexpected station {name}!");
                            }
                        }
                        RoutePoint::Transit { .. } => {
                            if !expect_station {
                                eprintln!("  transit");
                                expect_station = true;
                            } else {
                                panic!("unexpected transit!");
                            }
                        }
                        RoutePoint::Crossreference { .. } => {}
                        RoutePoint::Route { .. } => {
                            panic!("not fully denormalized! got route ref {point:?}");
                        }
                    }
                }
            }
        }
    }
}
