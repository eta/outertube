//! Processing intertube's network models.

#![allow(dead_code)]

use chrono::{NaiveDateTime, TimeDelta};
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize, Serializer};
use std::collections::{HashMap, HashSet, VecDeque};
use std::hash::{Hash, Hasher};
use std::ops::Deref;
use std::str::FromStr;

/// The number of seconds after the Unix epoch that the Common Lisp Universal Time epoch is at.
pub const UNIVERSAL_TS_SECONDS_AFTER_UNIX: u64 = 2208988800;

#[derive(Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ModelMetadata {
    generated_ts: u64,
    line_code: String,
}

/// The on-disk storage format for a `NetworkModel`.
#[derive(Deserialize, Clone, Debug)]
pub struct NetworkModelDisk {
    /// A map from model point names to track codes in that model point.
    codes: HashMap<String, Option<Vec<String>>>,
    /// A map from model point names to stations and platforms.
    stations: HashMap<String, (String, Option<String>)>,
    /// A map from model point names to links to other model points.
    links: HashMap<String, Option<Vec<String>>>,
    /// Information about the model.
    metadata: ModelMetadata,
}

/// A point in the network model.
#[derive(Clone, Debug)]
pub struct Modelpoint {
    /// The name of this model point.
    pub name: String,
    /// Ordered list of track codes.
    pub track_codes: Vec<String>,
    /// If this model point represents a station: the station name.
    pub station_name: Option<String>,
    /// If this model point represents a station: the platform (if it has one).
    pub platform: Option<String>,
    /// Other model points this one is connected to.
    pub links: Vec<String>,
}

impl Serialize for Modelpoint {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.name)
    }
}

impl Hash for Modelpoint {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

impl PartialEq for Modelpoint {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Eq for Modelpoint {}

impl Modelpoint {
    /// Get an ordered list of track codes in this modelpoint.
    pub fn track_codes(&self) -> &[String] {
        &self.track_codes
    }

    /// Get the station name and platform for this model point, if it is a station.
    pub fn station_details(&self) -> Option<(&String, Option<&String>)> {
        self.station_name
            .as_ref()
            .map(|sn| (sn, self.platform.as_ref()))
    }

    /// Get a list of other model points this one links to.
    pub fn links(&self) -> &[String] {
        &self.links
    }
}

/// A model of locations in a London Underground line.
pub struct NetworkModel {
    generated_at: NaiveDateTime,
    line: String,
    points: HashMap<String, Modelpoint>,
    code_to_points: HashMap<String, Vec<String>>,
    station_to_points: HashMap<(String, Option<String>), HashSet<String>>,
    station_name_to_points: HashMap<String, HashSet<String>>,
}

impl NetworkModel {
    /// Get the network model for the given `line` code.
    pub fn for_line(line: &str) -> Option<&'static Self> {
        // This is a bit tedious, but oh well.
        static MODEL_B_JSON: &str = include_str!("../models/B.json");
        static MODEL_C_JSON: &str = include_str!("../models/C.json");
        static MODEL_D_JSON: &str = include_str!("../models/D.json");
        static MODEL_H_JSON: &str = include_str!("../models/H.json");
        static MODEL_J_JSON: &str = include_str!("../models/J.json");
        static MODEL_M_JSON: &str = include_str!("../models/M.json");
        static MODEL_N_JSON: &str = include_str!("../models/N.json");
        static MODEL_P_JSON: &str = include_str!("../models/P.json");
        static MODEL_V_JSON: &str = include_str!("../models/V.json");
        static MODEL_W_JSON: &str = include_str!("../models/W.json");
        static MODEL_X_JSON: &str = include_str!("../models/X.json");

        lazy_static! {
            static ref MODEL_B: NetworkModel = MODEL_B_JSON.parse().unwrap();
            static ref MODEL_C: NetworkModel = MODEL_C_JSON.parse().unwrap();
            static ref MODEL_D: NetworkModel = MODEL_D_JSON.parse().unwrap();
            static ref MODEL_H: NetworkModel = MODEL_H_JSON.parse().unwrap();
            static ref MODEL_J: NetworkModel = MODEL_J_JSON.parse().unwrap();
            static ref MODEL_M: NetworkModel = MODEL_M_JSON.parse().unwrap();
            static ref MODEL_N: NetworkModel = MODEL_N_JSON.parse().unwrap();
            static ref MODEL_P: NetworkModel = MODEL_P_JSON.parse().unwrap();
            static ref MODEL_V: NetworkModel = MODEL_V_JSON.parse().unwrap();
            static ref MODEL_W: NetworkModel = MODEL_W_JSON.parse().unwrap();
            static ref MODEL_X: NetworkModel = MODEL_X_JSON.parse().unwrap();
        }

        match line {
            "B" => Some(MODEL_B.deref()),
            "C" => Some(MODEL_C.deref()),
            "D" => Some(MODEL_D.deref()),
            "H" => Some(MODEL_H.deref()),
            "J" => Some(MODEL_J.deref()),
            "M" => Some(MODEL_M.deref()),
            "N" => Some(MODEL_N.deref()),
            "P" => Some(MODEL_P.deref()),
            "V" => Some(MODEL_V.deref()),
            "W" => Some(MODEL_W.deref()),
            "X" => Some(MODEL_X.deref()),
            _ => None,
        }
    }

    /// Get the time this model was generated at.
    pub fn generated_at(&self) -> NaiveDateTime {
        self.generated_at
    }

    /// Get the line code this model is for.
    pub fn line(&self) -> &str {
        &self.line
    }

    /// Get a model point by name.
    //
    // TODO(eta): provide an unwrapping version with nice diagnostics
    pub fn get(&self, point: &str) -> Option<&Modelpoint> {
        self.points.get(point)
    }

    /// Get a list of model points that include a track code.
    pub fn get_by_track(&self, track_code: &str) -> Vec<&Modelpoint> {
        self.code_to_points
            .get(track_code)
            .iter()
            .copied()
            .flatten()
            .map(|x| &self.points[x])
            .collect()
    }

    /// Get a list of model points associated with a station and platform.
    pub fn get_by_station(&self, station: String, platform: Option<String>) -> Vec<&Modelpoint> {
        self.station_to_points
            .get(&(station, platform))
            .iter()
            .copied()
            .flatten()
            .map(|x| &self.points[x])
            .collect()
    }

    /// Get a set of model points associated with a station.
    pub fn get_all_by_station(&self, station: &str) -> HashSet<&Modelpoint> {
        self.station_name_to_points
            .get(station)
            .iter()
            .copied()
            .flatten()
            .map(|x| &self.points[x])
            .collect()
    }

    pub fn route_between<'a>(
        &'a self,
        from: &'a Modelpoint,
        to: &'a Modelpoint,
        max_depth: usize,
    ) -> Option<Vec<&'a Modelpoint>> {
        // This is mostly just a textbook breadth-first-search, except we can stop based on depth.

        let mut queue: VecDeque<(usize, &str)> = VecDeque::new();
        let mut explored = HashSet::new();
        let mut parents: HashMap<&str, &str> = HashMap::new();
        queue.push_back((0, &from.name));
        explored.insert(&from.name);

        while let Some((depth, node)) = queue.pop_front() {
            if node == to.name {
                let mut ret = VecDeque::new();
                ret.push_back(to);
                let mut cur = node;
                while let Some(parent) = parents.get(cur) {
                    ret.push_front(self.get(parent).unwrap());
                    cur = parent;
                }
                assert_eq!(ret[0], from);
                return Some(ret.into());
            }
            if depth < max_depth {
                for neighbour in self.get(node).map(|x| x.links()).unwrap_or(&[]) {
                    if !explored.contains(neighbour) && self.get(neighbour).is_some() {
                        explored.insert(neighbour);
                        parents.insert(neighbour, node);
                        queue.push_back((depth + 1, neighbour));
                    }
                }
            }
        }

        None
    }
}

impl FromStr for NetworkModel {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let disk: NetworkModelDisk = serde_json::from_str(s)?;
        let mut points = HashMap::new();
        let mut code_to_points: HashMap<String, Vec<String>> = HashMap::new();
        let mut station_to_points: HashMap<(String, Option<String>), HashSet<String>> =
            HashMap::new();
        let mut station_name_to_points: HashMap<String, HashSet<String>> = HashMap::new();

        for (point, codes) in disk.codes {
            let links = disk
                .links
                .get(&point)
                .cloned()
                .unwrap_or_default()
                .unwrap_or_default();
            let (station_name, platform) = disk
                .stations
                .get(&point)
                .map(|(station, pfm)| (Some(station.clone()), pfm.clone()))
                .unwrap_or((None, None));

            for code in codes.iter().flatten() {
                code_to_points
                    .entry(code.clone())
                    .or_default()
                    .push(point.clone());
            }

            if let Some(station) = station_name.clone() {
                station_to_points
                    .entry((station.clone(), platform.clone()))
                    .or_default()
                    .insert(point.clone());
                station_name_to_points
                    .entry(station)
                    .or_default()
                    .insert(point.clone());
            }

            points.insert(
                point.clone(),
                Modelpoint {
                    name: point,
                    track_codes: codes.unwrap_or_default(),
                    station_name,
                    platform,
                    links,
                },
            );
        }

        Ok(Self {
            generated_at: NaiveDateTime::UNIX_EPOCH
                + TimeDelta::seconds(
                    (disk.metadata.generated_ts - UNIVERSAL_TS_SECONDS_AFTER_UNIX) as i64,
                ),
            line: disk.metadata.line_code,
            points,
            code_to_points,
            station_to_points,
            station_name_to_points,
        })
    }
}

#[cfg(test)]
mod test {
    use crate::model::{Modelpoint, NetworkModel};
    use chrono::NaiveDateTime;
    use std::collections::HashMap;

    #[test]
    fn models_exist() {
        for line in ["B", "C", "D", "H", "J", "M", "N", "P", "V", "W", "X"] {
            NetworkModel::for_line(line);
        }
    }

    #[test]
    fn path_between() {
        let mut fake_model = NetworkModel {
            generated_at: NaiveDateTime::UNIX_EPOCH,
            line: "Z".to_string(),
            code_to_points: Default::default(),
            points: HashMap::new(),
            station_to_points: HashMap::new(),
            station_name_to_points: HashMap::new(),
        };

        let mut insert = |name: &'static str, links: Vec<&'static str>| {
            fake_model.points.insert(
                name.into(),
                Modelpoint {
                    name: name.into(),
                    track_codes: vec![],
                    station_name: None,
                    platform: None,
                    links: links.into_iter().map(|x| x.to_string()).collect(),
                },
            );
        };

        // 1-1 -> 2-1 -> 3-1 -> 4-1
        //     -> 2-2 -> 3-2
        //            -> 3-3
        // also 2-2 links to itself
        insert("1-1", vec!["2-1", "2-2"]);

        insert("2-1", vec!["3-1", "3-2"]);
        insert("2-2", vec!["2-2", "3-2", "3-3"]);

        insert("3-1", vec!["4-1"]);
        insert("3-2", vec![]);
        insert("3-3", vec![]);

        insert("4-1", vec![]);

        let route_between = |from, to, max_depth| {
            fake_model
                .route_between(
                    fake_model.get(from).unwrap(),
                    fake_model.get(to).unwrap(),
                    max_depth,
                )
                .unwrap_or_default()
                .iter()
                .map(|x| &x.name as &str)
                .collect::<Vec<&str>>()
        };

        assert_eq!(
            route_between("1-1", "4-1", usize::MAX),
            vec!["1-1", "2-1", "3-1", "4-1"]
        );

        assert_eq!(route_between("1-1", "4-1", 2), Vec::<&str>::new());

        assert_eq!(route_between("1-1", "3-3", 2), vec!["1-1", "2-2", "3-3"]);
    }
}
