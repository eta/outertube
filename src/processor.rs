//! Processing trackernet observations into usable data.
//!
//! Unlike intertube, we're going to start here by doing an extremely basic, non-concurrent
//! implementation and seeing how far that gets us.

use crate::config::ProcessorConfig;
use crate::metrics::TRAINS_COUNT;
use crate::model::{Modelpoint, NetworkModel};
use crate::storage::TrainData;
use crate::trackernet::{TrackernetObservation, TrackernetTrain};
use anyhow::{anyhow, Context};
use async_broadcast::Sender;
use base64::prelude::BASE64_URL_SAFE_NO_PAD;
use base64::Engine;
use chrono::{DateTime, Utc};
use chrono_tz::{Europe, Tz};
use lazy_static::lazy_static;
use regex::Regex;
use serde::{Deserialize, Serialize};
use slog::{debug, info, o, trace, warn, Logger};
use std::borrow::Cow;
use std::collections::{HashMap, HashSet, VecDeque};
use std::convert::Infallible;
use std::fs::File;
use std::str::FromStr;
use std::sync::Arc;
use std::time::Duration;
use uuid::Uuid;

/// How long it takes for a train to be considered stale.
///
/// If a train is stale, we aren't getting updates from it any more, and we should consider that
/// it might have changed identifier to another train. The rescuer handles figuring that out.
const TRAIN_STALE_DURATION: Duration = Duration::from_secs(30);
/// How long it takes for a train to be considered expired.
///
/// Expired trains are removed from the dataset and archived (eventually, to Clickhouse).
const TRAIN_EXPIRY_DURATION: Duration = Duration::from_secs(360);
/// How long it takes for a train to be considered "too old".
///
/// This exists so trains eventually get archived (otherwise, they'll grow too large and exhaust
/// available memory).
const TRAIN_MAX_AGE: Duration = Duration::from_secs(60 * 60 * 48);
/// How long after `TRAIN_STALE_DURATION` to wait before a train can serve as a rescue target.
///
/// NOTE(eta): I don't think this is entirely necessary, but the old code has it. Probably it's
/// there to account for scrape timing differences, and can be lowered to more like 5 seconds.
const TRAIN_RESCUE_GRACE_PERIOD: Duration = Duration::from_secs(15);
/// Line codes that don't start a new trip when their identity changes.
const UNRELIABLE_IDENTITY_CHANGE_LINES: [&str; 2] = ["D", "M"];

/// A processor capable of turning observations into structured train data.
pub struct Processor {
    train_data: Arc<TrainData>,
    description_overrides: HashMap<String, HashMap<String, String>>,
    config: ProcessorConfig,
    events_tx: Sender<ProcessorEvent>,
}

impl Processor {
    pub fn new(
        train_data: Arc<TrainData>,
        config: ProcessorConfig,
        events_tx: Sender<ProcessorEvent>,
    ) -> anyhow::Result<Self> {
        let description_overrides = if let Some(ref path) = config.descriptions_override_path {
            let file = File::open(path).context("failed to open descriptions override path")?;
            serde_json::from_reader(file).context("failed to deserialize description overrides")?
        } else {
            HashMap::new()
        };
        Ok(Self {
            train_data,
            description_overrides,
            config,
            events_tx,
        })
    }

    pub fn process(&mut self, observations: TrackernetObservation, log: &Logger) {
        for mut obs in observations.trains {
            let key = (obs.line_code.clone(), obs.trackernet_key());
            let mut train_data = self.train_data.inner.write().unwrap();
            let train = if let Some(tid) = { train_data.key_map.get(&key).cloned() } {
                if let Some(t) = train_data.trains.get_mut(&tid) {
                    t
                } else {
                    panic!("inconsistent key_map: lookup of {key:?} returned nonexistent train {tid:?}")
                }
            } else {
                let train = Train::new(obs.line_code.clone());
                let tid = train.unique_id.clone();
                train_data.trains.insert(tid.clone(), train);
                info!(log, "new train for {} train {}", key.0, key.1; "train_id" => tid.clone());
                let _ = self.events_tx.broadcast_blocking(ProcessorEvent::New {
                    train_id: tid.clone(),
                    line_code: key.0.clone(),
                    trackernet_key: key.1.clone(),
                });
                train_data.key_map.insert(key, tid.clone());
                TRAINS_COUNT.set(train_data.trains.len() as f64);
                train_data.trains.get_mut(&tid).unwrap()
            };
            if let Some(new_desc) = self
                .description_overrides
                .get(&obs.line_code)
                .and_then(|table| table.get(&obs.track_code))
            {
                obs.overridden_location_desc = Some(new_desc.to_owned());
            }
            let logger = log
                .new(o!("train_id" => train.unique_id.clone(), "line" => train.line_code.clone()));
            train.apply_observation(
                obs.clone(),
                observations.timestamp,
                &self.events_tx,
                &logger,
            );
            let rescuable_end = train.first_updated + TRAIN_EXPIRY_DURATION;
            let tid = train.unique_id().to_owned();
            // If we're configured to, throw away all raw trackernet data after the train becomes
            // rescuable.
            if Utc::now() > rescuable_end && self.config.discard_observations {
                train_data.raw_observations.remove(&tid);
            } else {
                train_data
                    .raw_observations
                    .entry(tid)
                    .or_default()
                    .push((observations.timestamp.with_timezone(&Utc), obs));
            }
        }
    }
}

/// A London Underground train.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Train {
    /// A unique identifier for the train.
    unique_id: String,
    /// Trackernet's identifier for this train (which can change).
    trackernet_key: String,
    /// The line code for this train.
    line_code: String,
    /// The train's timetable number.
    train_number: Option<u16>,
    /// Which trip this train is doing.
    trip_number: Option<u16>,
    /// The leading car number of this train.
    leading_car_no: Option<String>,
    /// When the train was first created.
    first_updated: DateTime<Utc>,
    /// When the train was last created.
    last_updated: DateTime<Utc>,
    /// Where the train is going.
    destination: Destination,
    /// What branch the train is on.
    branch: Option<String>,
    /// Where the train is and has been.
    locations: Vec<Location>,
    /// Where we think the train will go.
    future_locations: VecDeque<FutureLocation>,
    /// Things that have happened on the train's journey.
    events: Vec<HistoryEvent>,
    /// A list of previous trips this train has taken.
    trips: Vec<Trip>,
}

impl Train {
    fn generate_unique_id() -> String {
        let uuid = Uuid::new_v4();
        let uuid_encoded = BASE64_URL_SAFE_NO_PAD.encode(uuid);
        format!("train_{uuid_encoded}")
    }

    pub fn new(line_code: String) -> Self {
        Self {
            unique_id: Self::generate_unique_id(),
            trackernet_key: "".to_string(),
            line_code,
            train_number: None,
            trip_number: None,
            leading_car_no: None,
            first_updated: DateTime::<Utc>::MIN_UTC,
            last_updated: DateTime::<Utc>::MIN_UTC,
            destination: Default::default(),
            branch: None,
            locations: vec![],
            future_locations: VecDeque::new(),
            events: vec![],
            trips: vec![],
        }
    }

    pub fn new_historical(
        observations: Vec<(DateTime<Utc>, TrackernetTrain)>,
    ) -> anyhow::Result<Self> {
        let line_code = observations
            .first()
            .ok_or_else(|| anyhow!("new_historical called with empty list"))?
            .1
            .line_code
            .clone();

        let mut ret = Self::new(line_code);
        let fakelog = Logger::root(slog::Discard, o!());

        for (ts, observation) in observations {
            let events = ret.events_from_observation(
                observation.clone(),
                ts.with_timezone(&Europe::London),
                &fakelog,
                false,
            );
            events.into_iter().for_each(|evt| ret.apply_event(evt));
        }

        Ok(ret)
    }

    pub fn unique_id(&self) -> &str {
        &self.unique_id
    }

    pub fn line_code(&self) -> &str {
        &self.line_code
    }

    pub fn trackernet_key(&self) -> &str {
        &self.trackernet_key
    }

    pub fn identity_key(&self) -> (String, String) {
        (self.line_code.clone(), self.trackernet_key.clone())
    }

    pub fn train_number(&self) -> Option<u16> {
        self.train_number
    }

    pub fn destination(&self) -> &Destination {
        &self.destination
    }

    pub fn at_destination(&self) -> bool {
        if let Some(curloc) = self.current_location() {
            if let LocationKind::Station {
                name: ref cur_name, ..
            } = curloc.kind
            {
                if let Some(LocationKind::Station {
                    name: ref dest_name,
                    ..
                }) = self.destination.location_kind
                {
                    return cur_name == dest_name;
                }
                if cur_name == &self.destination.text {
                    return true;
                }
            }
        }
        false
    }

    pub fn next_location_kind(&self) -> Option<Cow<'_, LocationKind>> {
        if let Some(l) = self.future_locations.front() {
            Some(Cow::Borrowed(&l.kind))
        } else if let (Some(cur), Some(model)) = (
            self.current_location(),
            NetworkModel::for_line(self.line_code()),
        ) {
            if cur.model_point.len() != 1 {
                None
            } else {
                let point = model.get(cur.model_point.iter().next().unwrap()).unwrap();
                (point.links.len() == 1)
                    .then(|| model.get(&point.links[0]).unwrap())
                    .map(|p| Cow::Owned(LocationKind::from(p)))
            }
        } else {
            None
        }
    }

    pub fn locations(&self) -> &[Location] {
        &self.locations
    }

    pub fn future_locations(&self) -> &VecDeque<FutureLocation> {
        &self.future_locations
    }

    pub fn current_trip_id(&self) -> usize {
        self.trips.len()
    }

    pub fn trips(&self) -> &[Trip] {
        &self.trips
    }

    pub fn events(&self) -> &[HistoryEvent] {
        &self.events
    }

    pub fn leading_car_no(&self) -> Option<&str> {
        self.leading_car_no.as_deref()
    }

    pub fn first_updated(&self) -> DateTime<Utc> {
        self.first_updated
    }

    pub fn last_updated(&self) -> DateTime<Utc> {
        self.last_updated
    }

    pub fn first_updated_london(&self) -> DateTime<Tz> {
        self.first_updated.with_timezone(&Europe::London)
    }

    pub fn last_updated_london(&self) -> DateTime<Tz> {
        self.last_updated.with_timezone(&Europe::London)
    }

    pub fn current_location(&self) -> Option<&Location> {
        self.locations.last()
    }

    pub fn current_description(&self) -> Cow<'_, str> {
        self.current_location()
            .map(|x| x.kind.current_description())
            .unwrap_or("<brand new train>".into())
    }

    pub fn current_model_points(&self) -> Option<HashSet<&Modelpoint>> {
        let loc = self.current_location()?;
        let model = NetworkModel::for_line(&self.line_code)?;
        let mut ret = HashSet::new();
        ret.extend(loc.model_point.iter().flat_map(|p| model.get(p)));
        Some(ret)
    }

    pub fn first_location(&self) -> Option<&Location> {
        self.locations.first()
    }

    pub fn current_track_code(&self) -> Option<&str> {
        self.locations
            .last()
            .and_then(|loc| loc.track_codes.last().map(|x| x as &str))
    }

    pub fn first_track_code(&self) -> Option<&str> {
        self.locations
            .first()
            .and_then(|loc| loc.track_codes.first().map(|x| x as &str))
    }

    /// Checks whether this train is stale at the provided time.
    pub fn is_stale(&self, now: DateTime<Utc>) -> bool {
        self.last_updated <= (now - TRAIN_STALE_DURATION)
    }

    /// Checks whether the train is expired at the provided time.
    pub fn is_expired(&self, now: DateTime<Utc>) -> bool {
        self.last_updated <= (now - TRAIN_EXPIRY_DURATION)
            || self.first_updated <= (now - TRAIN_MAX_AGE)
    }

    /// Checks whether the train is a viable rescue target.
    ///
    /// Trains only become rescuable after the stale duration (plus a grace period), so that
    /// the rescue source has a chance to become stale enough to be considered for rescuing
    /// (if we didn't do this, the train could get incorrectly snapped up by a much older train!).
    /// We also don't allow rescues to happen after the expiry duration for similar reasons.
    pub fn is_rescue_target(&self, now: DateTime<Utc>) -> bool {
        let rescuable_start = self.first_updated + TRAIN_STALE_DURATION + TRAIN_RESCUE_GRACE_PERIOD;
        // NOTE(eta): duplicated in the throwing-away logic
        let rescuable_end = self.first_updated + TRAIN_EXPIRY_DURATION;
        now >= rescuable_start && now <= rescuable_end
    }

    fn events_from_observation(
        &self,
        observation: TrackernetTrain,
        timestamp: DateTime<Tz>,
        log: &Logger,
        is_live: bool,
    ) -> Vec<HistoryEvent> {
        let mut ret = vec![];
        let timestamp = timestamp.with_timezone(&Utc);

        // The current trip id.
        let mut trip_id = self.current_trip_id();
        // All trip ids used for this observation. (This exists so if we start a new trip and a new
        // location in the same observation, it gets logged with both trips.)
        let mut trip_ids = vec![trip_id];

        // NOTE(eta): We do the skip-back-in-time check *after* generating this event so that
        //            rescues work (otherwise, it wouldn't change its trackernet_key).
        let trackernet_key = observation.trackernet_key();
        if trackernet_key != self.trackernet_key {
            ret.push(
                HistoryEventContent::Rescue {
                    new_key: trackernet_key,
                }
                .with_time_and_trip(timestamp, trip_id),
            );
        }

        if timestamp <= self.last_updated {
            // FIXME(eta): investigate why this happens so often
            trace!(
                log,
                "Rejecting skip back in time ({}sec)",
                (self.last_updated - timestamp).num_seconds()
            );
            return ret;
        }

        if (observation.set_no, observation.trip_no) != (self.train_number, self.trip_number) {
            // FIXME(eta): This is very simplistic; if a train gets rerouted en route that'll start
            //             a new trip, for example! Ideally we'd link this to turnbacks, but this
            //             will have to do for now.
            if self.train_number.is_some()
                && self.current_location().is_some()
                && !UNRELIABLE_IDENTITY_CHANGE_LINES.contains(&self.line_code())
            {
                trip_id += 1;
                trip_ids.push(trip_id);
                // NOTE(eta): We defer the NewTrip event until after the new location, which is
                //            absolutely a horrible bodge but oh well.
            }

            ret.push(
                HistoryEventContent::IdentityChange {
                    wtt_id: observation.set_no,
                    wtt_trip: observation.trip_no,
                }
                .with_time_and_trip(timestamp, trip_id),
            );
        }
        let leading_car_no = if observation.leading_car_no == "0" {
            None
        } else {
            Some(observation.leading_car_no)
        };
        if self.leading_car_no != leading_car_no {
            ret.push(
                HistoryEventContent::CarChange { leading_car_no }
                    .with_time_and_trip(timestamp, trip_id),
            );
        }

        let modelpoints = NetworkModel::for_line(&self.line_code)
            .map(|model| model.get_by_track(&observation.track_code))
            .unwrap_or_default();
        let modelpoint_names = modelpoints
            .iter()
            .map(|x| x.name.clone())
            .collect::<HashSet<_>>();

        // FIXME(eta): implement this properly with input_dest parsing etc
        let destination = NetworkModel::for_line(&self.line_code)
            .map(|model| Destination::from_text_with_model(&observation.destination_desc, model))
            .unwrap_or_else(|| Destination::from_text_basic(observation.destination_desc));

        let mut new_route = false;

        if self.destination != destination {
            if is_live {
                let future_locations = NetworkModel::for_line(&self.line_code)
                    .and_then(|model| destination.route_from(&modelpoint_names, model, log))
                    .unwrap_or_default();
                new_route = true;

                ret.push(
                    HistoryEventContent::RouteChange { future_locations }
                        .with_time_and_trip(timestamp, trip_id),
                );
            }
            ret.push(
                HistoryEventContent::DestinationChange {
                    destination: destination.clone(),
                    minor: false,
                }
                .with_time_and_trip(timestamp, trip_id),
            );
        }

        let location_desc = observation
            .overridden_location_desc
            .or(observation.location_desc)
            .unwrap_or_else(|| format!("Unknown location ({})", observation.track_code));

        let location_parsed = LocationKind::from_str(&location_desc).unwrap();
        let location_from_model = if modelpoints.is_empty() {
            None
        } else if modelpoints.len() == 1 {
            let model_point = modelpoints.into_iter().next().unwrap();
            let track_code_index = model_point
                .track_codes
                .iter()
                .position(|t| t == &observation.track_code)
                .unwrap();
            let progress_perc =
                (track_code_index as f32) * 100.0 / (model_point.track_codes.len() as f32);
            Some(if let Some(station) = model_point.station_name.clone() {
                LocationKind::Station {
                    name: station,
                    platform: model_point.platform.clone(),
                }
            } else {
                LocationKind::Transit {
                    description: Some(location_desc.to_owned()),
                    progress_perc: Some(progress_perc),
                }
            })
        } else {
            // If there's more than one modelpoint, we assume it's Transit since stations
            // should be unambiguous.
            // FIXME(eta): maybe revisit this assumption
            Some(LocationKind::Transit {
                description: Some(location_desc.to_owned()),
                progress_perc: None,
            })
        };

        // We only think we're in transit if both the parsed location and the model location
        // think we are. If one thinks we're in a station (or other location), we prefer that
        // (and log a warning if the model needs improving).
        let location_kind = match (location_parsed, location_from_model) {
            (x, None) => x,
            (_, Some(station @ LocationKind::Station { .. })) => station,
            (_, Some(other @ LocationKind::Other { .. })) => other,
            (LocationKind::Station { name, platform }, Some(LocationKind::Transit { .. })) => {
                // If we've just moved here, log a warning (but don't if we're sitting here, that's too spammy)
                if self
                    .current_track_code()
                    .map(|x| x != observation.track_code)
                    .unwrap_or(true)
                {
                    warn!(
                        log,
                        "track code {} parses as station {:?}{}, but model thinks transit",
                        observation.track_code,
                        name,
                        platform
                            .as_ref()
                            .map(|x| format!(" (pfm {x:?})"))
                            .unwrap_or(String::new());
                        "model_points" => format!("{:?}", modelpoint_names)
                    );
                }
                LocationKind::Station { name, platform }
            }
            (_, Some(transit @ LocationKind::Transit { .. })) => transit,
        };

        if self
            .current_location()
            .map(|x| x.kind != location_kind)
            .unwrap_or(true)
        {
            trace!(log, "new location: {location_kind:?}");

            // Let's figure out whether this new location means we should advance our list of
            // predicted future locations.
            let mut advance_future = false;

            if let Some(next_loc) = self.future_locations.front() {
                // We only do the calculation if the routes haven't changed this run.
                if !new_route {
                    if modelpoint_names.contains(&next_loc.model_point) {
                        // Our future prediction was correct! Pop off the first predicted future
                        // location, since we're there now.
                        advance_future = true;

                        crate::metrics::ACCURATE_ROUTE_COUNTER
                            .with_label_values(&[&self.line_code])
                            .inc();
                    } else {
                        // Our future prediction failed; try and recalculate the route with this
                        // new information.
                        let future_locations = NetworkModel::for_line(&self.line_code)
                            .and_then(|model| destination.route_from(&modelpoint_names, model, log))
                            .unwrap_or_default();

                        if future_locations.is_empty() {
                            // We couldn't recalculate it, so this train's just lost routing data
                            // for now.
                            warn!(
                                log,
                                "train lost route";
                                "expected" => next_loc.model_point.clone(),
                                "actual" => format!("{:?}", modelpoint_names)
                            );

                            crate::metrics::LOST_ROUTE_COUNTER
                                .with_label_values(&[&self.line_code, &observation.track_code])
                                .inc();
                        } else {
                            debug!(
                                log,
                                "rerouting required";
                                "expected" => next_loc.model_point.clone(),
                                "actual" => format!("{:?}", modelpoint_names)
                            );

                            crate::metrics::REROUTE_COUNTER
                                .with_label_values(&[&self.line_code, &observation.track_code])
                                .inc();
                        }

                        ret.push(
                            HistoryEventContent::RouteChange { future_locations }
                                .with_time_and_trip(timestamp, trip_id),
                        );
                    }
                }
            } else if !new_route && !is_live {
                let future_locations = NetworkModel::for_line(&self.line_code)
                    .and_then(|model| destination.route_from(&modelpoint_names, model, log))
                    .unwrap_or_default();

                if !future_locations.is_empty() {
                    debug!(
                        log,
                        "route regained";
                    );
                    crate::metrics::ROUTE_REGAIN_COUNTER
                        .with_label_values(&[&self.line_code, &observation.track_code])
                        .inc();
                }

                ret.push(
                    HistoryEventContent::RouteChange { future_locations }
                        .with_time_and_trip(timestamp, trip_id),
                );
            }

            ret.push(
                HistoryEventContent::NewLocation {
                    new_location: Location {
                        kind: location_kind,
                        model_point: modelpoint_names,
                        track_codes: vec![observation.track_code],
                        start_ts: timestamp,
                        end_ts: timestamp,
                        trip_id: trip_ids.iter().copied().collect(),
                    },
                    advance_future,
                }
                .with_time_and_trip(timestamp, trip_id),
            );
        } else {
            let modelpoint_change = self
                .current_location()
                .map(|x| x.model_point != modelpoint_names)
                .unwrap_or(true);
            let description_change = self
                .current_location()
                .map(|x| x.kind.transit_description() != location_kind.transit_description())
                .unwrap_or(true);
            let progress_perc = if let LocationKind::Transit { progress_perc, .. } = location_kind {
                progress_perc
            } else {
                None
            };
            ret.push(
                HistoryEventContent::Transit {
                    track_code: observation.track_code,
                    model_point: modelpoint_change.then_some(modelpoint_names),
                    description: description_change
                        .then(|| location_kind.transit_description().map(ToOwned::to_owned))
                        .flatten(),
                    progress_perc,
                }
                .with_time_and_trip(timestamp, trip_id),
            );
        }

        // Check if we started a new trip this run and generate an event if so.
        if trip_ids.len() > 1 {
            ret.push(HistoryEventContent::NewTrip.with_time_and_trip(timestamp, trip_id));
        }

        ret
    }

    fn apply_event(&mut self, event: HistoryEvent) {
        if self.first_updated == DateTime::<Utc>::MIN_UTC {
            self.first_updated = event.timestamp;
        }

        if event.timestamp >= self.last_updated {
            self.last_updated = event.timestamp;
        }

        let should_record = event.content.is_recordable();
        let current_trip_id = self.current_trip_id();

        match event.content.clone() {
            HistoryEventContent::Transit {
                track_code,
                model_point,
                description,
                progress_perc: p,
            } => {
                let loc = self
                    .locations
                    .last_mut()
                    .expect("Transit event generated for empty locations list");

                loc.trip_id.insert(current_trip_id);

                if loc.track_codes.last() != Some(&track_code) {
                    loc.track_codes.push(track_code);
                }
                if let Some(m) = model_point {
                    loc.model_point = m;
                }
                if let Some(d) = description {
                    if let LocationKind::Transit {
                        ref mut description,
                        ref mut progress_perc,
                        ..
                    } = loc.kind
                    {
                        *description = Some(d);
                        *progress_perc = p;
                    }
                }
                loc.end_ts = event.timestamp;
            }
            HistoryEventContent::NewLocation {
                new_location,
                advance_future,
            } => {
                if let Some(Location {
                    kind: LocationKind::Transit { description, .. },
                    ..
                }) = self.locations.last_mut()
                {
                    // Empty out the description for historical transit locations.
                    *description = None;
                }
                self.locations.push(new_location);
                if advance_future {
                    self.future_locations.pop_front();
                }
            }
            HistoryEventContent::NewTrip => {
                let current_trip_id = self.current_trip_id();
                let first_location = self
                    .locations
                    .iter()
                    .find(|l| l.trip_id.contains(&current_trip_id))
                    .expect("could not find first location for trip change");
                let last_location = self
                    .locations
                    .last()
                    .expect("could not find last location for trip change");

                self.trips.push(Trip {
                    start: first_location.kind.clone(),
                    start_time: first_location.start_ts,
                    end: last_location.kind.clone(),
                    end_time: last_location.end_ts,
                    train_number: self.train_number,
                    trip_number: self.trip_number,
                });
            }
            HistoryEventContent::IdentityChange { wtt_id, wtt_trip } => {
                self.train_number = wtt_id;
                self.trip_number = wtt_trip;
            }
            HistoryEventContent::CarChange { leading_car_no } => {
                self.leading_car_no = leading_car_no;
            }
            HistoryEventContent::DestinationChange {
                destination,
                minor: _minor,
            } => {
                self.destination = destination;
            }
            HistoryEventContent::RouteChange { future_locations } => {
                self.future_locations = future_locations.into();
            }
            HistoryEventContent::BranchChange { branch } => {
                self.branch = branch;
            }
            HistoryEventContent::Rescue { new_key } => {
                self.trackernet_key = new_key;
            }
            HistoryEventContent::InService => {}
            HistoryEventContent::OutOfService => {}
        }

        if should_record {
            self.events.push(event);
        }
    }

    pub fn apply_observation(
        &mut self,
        observation: TrackernetTrain,
        timestamp: DateTime<Tz>,
        event_tx: &Sender<ProcessorEvent>,
        log: &Logger,
    ) -> Vec<HistoryEvent> {
        let events = self.events_from_observation(observation.clone(), timestamp, log, true);
        let ret = events.clone();
        let count = events.len();
        events.into_iter().for_each(|evt| {
            let _ = event_tx.broadcast_blocking(ProcessorEvent::Update {
                train_id: self.unique_id.clone(),
                line_code: self.line_code.clone(),
                event: evt.clone(),
            });
            self.apply_event(evt)
        });
        trace!(log, "processed {count} events");
        ret
    }
}

/// Somewhere a train is going.
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct Destination {
    /// The raw destination text from Trackernet.
    pub text: String,
    /// Structured information about the destination (if we can get it).
    pub location_kind: Option<LocationKind>,
    /// A set of model points representing 'via' points.
    ///
    /// If these exist, a route to the destination must pass through one of them.
    pub via: HashSet<String>,
    /// A set of model points representing the destination.
    pub model_point: HashSet<String>,
}

impl Destination {
    fn from_text_basic(text: String) -> Self {
        Self {
            text,
            ..Default::default()
        }
    }
    fn from_text_with_model(text: &str, model: &NetworkModel) -> Self {
        let mut split = text.split(" via ");
        let dest = split.next().unwrap();
        let via_part = split.next();

        let model_point = model
            .get_all_by_station(dest)
            .into_iter()
            .map(|x| x.name.clone())
            .collect::<HashSet<_>>();
        let via = via_part
            .into_iter()
            .flat_map(|x| {
                model
                    .get_all_by_station(x)
                    .into_iter()
                    .map(|y| y.name.clone())
            })
            .collect::<HashSet<_>>();

        let location_kind = if !model_point.is_empty() {
            Some(LocationKind::Station {
                name: text.to_owned(),
                platform: None,
            })
        } else {
            None
        };

        Self {
            text: text.to_owned(),
            location_kind,
            via,
            model_point,
        }
    }

    fn routes_between<'a, 'b>(
        from: impl Iterator<Item = &'a String> + Clone,
        to: impl Iterator<Item = &'a String> + Clone,
        model: &'b NetworkModel,
    ) -> Vec<Vec<&'b Modelpoint>> {
        let from = from.map(|f| model.get(f).unwrap());
        let to = to.map(|t| model.get(t).unwrap());

        let cartesian_product = from.flat_map(move |f| to.clone().map(move |t| (f, t)));
        cartesian_product
            .flat_map(|(f, t)| {
                let ret = model.route_between(f, t, usize::MAX);
                if ret.is_none() {
                    crate::metrics::FAILED_ROUTING_COUNTER
                        .with_label_values(&[model.line(), &f.name, &t.name])
                        .inc()
                }
                ret
            })
            .collect::<Vec<_>>()
    }

    fn route_from(
        &self,
        from: &HashSet<String>,
        model: &NetworkModel,
        log: &Logger,
    ) -> Option<Vec<FutureLocation>> {
        let log = log.new(o!(
            "from" => format!("{:?}", from),
            "to" => format!("{:?}", self.model_point)
        ));

        if from.is_empty() || self.model_point.is_empty() {
            trace!(log, "routing: empty start point");
            return None;
        }

        if self.model_point.intersection(from).next().is_some() {
            trace!(log, "routing: already at destination");
            return Some(vec![]);
        }

        // Compute the shortest direct route between start and end, to start with.
        let mut direct_routes = Self::routes_between(from.iter(), self.model_point.iter(), model);
        direct_routes.sort_by_key(|r| r.len());
        let shortest_direct_route = direct_routes.into_iter().next();

        let mut routes_via = if !self.via.is_empty() {
            // If there's a via point, compute the routes to that point.
            let mut here_to_via = Self::routes_between(from.iter(), self.via.iter(), model);
            here_to_via.sort_by_key(|r| r.len());

            // Check that the shortest route to the via point is shorter than the direct route.
            // This is a crude heuristic, but it stops us from routing all the way 'back' past
            // the destination to get to the via point if we've already passed it.
            if let Some(shortest_via) = here_to_via.first() {
                let shortest_direct_len = shortest_direct_route
                    .as_ref()
                    .map(|r| r.len())
                    .unwrap_or(usize::MAX);

                if shortest_via.len() > shortest_direct_len {
                    debug!(
                        log,
                        "routing: {} steps to via, but only {} direct; ignoring via",
                        shortest_via.len(),
                        shortest_direct_len;
                        "via" => format!("{:?}", self.via)
                    );
                    vec![]
                } else {
                    // Return routes by calculating the routes from the via point to the end and
                    // concatenating them.
                    here_to_via
                        .into_iter()
                        .flat_map(move |to_via| {
                            Self::routes_between(
                                [&to_via.last().unwrap().name].into_iter(),
                                self.model_point.iter(),
                                model,
                            )
                            .into_iter()
                            .map(move |to_dest| {
                                let mut ret = to_via.clone();
                                ret.extend(to_dest[1..].iter().copied());
                                ret
                            })
                        })
                        .collect::<Vec<_>>()
                }
            } else {
                trace!(log, "routing: no routes with via point"; "via" => format!("{:?}", self.via));
                vec![]
            }
        } else {
            vec![]
        };

        routes_via.sort_by_key(|r| r.len());

        let route = if let Some(r) = routes_via.into_iter().next() {
            trace!(log, "routing: found via route, length {}", r.len(); "via" => format!("{:?}", self.via));
            r
        } else if let Some(r) = shortest_direct_route {
            trace!(log, "routing: found direct route, length {}", r.len());
            r
        } else {
            trace!(log, "routing: no route found");
            return None;
        };

        Some(
            route
                .into_iter()
                .skip(1)
                .map(|r| FutureLocation {
                    model_point: r.name.clone(),
                    kind: LocationKind::from(r),
                })
                .collect(),
        )
    }
}

/// A summary of a trip that happened in the train's past.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Trip {
    /// Where the trip started.
    pub start: LocationKind,
    /// When the trip started.
    pub start_time: DateTime<Utc>,
    /// Where the trip finished.
    pub end: LocationKind,
    /// When the trip finished.
    pub end_time: DateTime<Utc>,
    /// The timetable ID for the trip.
    pub train_number: Option<u16>,
    /// The timetable's trip number for this trip.
    pub trip_number: Option<u16>,
}

impl Trip {
    pub fn describe(&self) -> String {
        format!(
            "{} {} to {}",
            self.start_time
                .with_timezone(&Europe::London)
                .format("%H:%M"),
            self.start.station_name().unwrap_or("somewhere"),
            self.end.station_name().unwrap_or("somewhere")
        )
    }
}

/// Somewhere a train is or has been.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Location {
    /// The sort of location this is.
    pub kind: LocationKind,
    /// A corresponding point for this location on the network model.
    ///
    /// This is a `HashSet` to represent uncertainty as to which point the train is actually at.
    /// The model point might narrow itself down over time.
    pub model_point: HashSet<String>,
    /// The track codes observed while at this location.
    pub track_codes: Vec<String>,
    /// The start time of this location entry.
    pub start_ts: DateTime<Utc>,
    /// The end time of this location entry.
    pub end_ts: DateTime<Utc>,
    /// Which trip IDs this location is in.
    pub trip_id: HashSet<usize>,
}

/// Somewhere a train will be in the future.
///
/// Ideally we figure out how to estimate timings and put that in here later, too!
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct FutureLocation {
    /// The sort of location this is.
    pub kind: LocationKind,
    /// A corresponding point for this location on the network model.
    pub model_point: String,
}

/// A kind of `Location`.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum LocationKind {
    /// The train's at a station.
    Station {
        /// The name of the station.
        name: String,
        /// The platform in the station.
        platform: Option<String>,
    },
    /// The train's between two stations.
    Transit {
        /// A human-readable description of where the train currently is.
        ///
        /// Only filled if this location is still current.
        description: Option<String>,
        /// As a percentage, how much progress has been made towards the next location.
        progress_perc: Option<f32>,
    },
    /// The train's somewhere else, like a stabling point or sidings or something.
    Other {
        /// The name of the location.
        name: String,
    },
}

impl<'a> From<&'a Modelpoint> for LocationKind {
    fn from(p: &'a Modelpoint) -> Self {
        if let Some(ref station_name) = p.station_name {
            LocationKind::Station {
                name: station_name.clone(),
                platform: p.platform.clone(),
            }
        } else {
            LocationKind::Transit {
                description: None,
                progress_perc: None,
            }
        }
    }
}

impl PartialEq for LocationKind {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (
                LocationKind::Station { name, platform },
                LocationKind::Station {
                    name: name2,
                    platform: platform2,
                },
            ) => name == name2 && platform == platform2,
            (LocationKind::Transit { .. }, LocationKind::Transit { .. }) => true,
            (LocationKind::Other { name }, LocationKind::Other { name: name2 }) => name == name2,
            _ => false,
        }
    }
}

impl LocationKind {
    pub const EMPTY_TRANSIT: Self = LocationKind::Transit {
        description: None,
        progress_perc: None,
    };

    pub fn station_name(&self) -> Option<&str> {
        match self {
            LocationKind::Station { name, .. } => Some(&name),
            _ => None,
        }
    }
    pub fn current_description(&self) -> Cow<'_, str> {
        match self {
            LocationKind::Station { name, platform } => if let Some(pfm) = platform {
                format!("at {name} platform {pfm}")
            } else {
                format!("at {name}")
            }
            .into(),
            LocationKind::Transit {
                description,
                progress_perc,
            } => {
                if let Some(desc) = description {
                    if let Some(perc) = progress_perc {
                        format!("{} ({:.0}%)", desc, perc).into()
                    } else {
                        desc.into()
                    }
                } else {
                    "nowhere".into()
                }
            }
            LocationKind::Other { name } => format!("at {name}").into(),
        }
    }
    fn transit_description(&self) -> Option<&str> {
        match self {
            LocationKind::Transit { description, .. } => description.as_deref(),
            _ => None,
        }
    }
}

impl FromStr for LocationKind {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref REGEX_PLATFORM: Regex =
                Regex::new(r"At (.+) Plat?form ([^ ]+)( .*)?").unwrap();
            static ref REGEX_COMPONENTIZER: Regex = Regex::new(r"At (.+) (/[A-Z])").unwrap();
            static ref REGEX_COMPONENTIZER_TRACK_CODE: Regex =
                Regex::new(r"At (.+) - ([A-Z0-9]+)").unwrap();
        }
        if let Some(caps) = REGEX_PLATFORM.captures(s) {
            Ok(Self::Station {
                name: caps[1].to_owned(),
                platform: caps.get(2).map(|x| x.as_str().to_owned()),
            })
        } else if let Some(caps) = REGEX_COMPONENTIZER.captures(s) {
            Ok(Self::Other {
                name: caps[1].to_owned(),
            })
        } else if let Some(caps) = REGEX_COMPONENTIZER_TRACK_CODE.captures(s) {
            Ok(Self::Other {
                name: caps[1].to_owned(),
            })
        } else if let Some(other) = (s != "At Platform")
            .then(|| s.strip_prefix("At "))
            .flatten()
        {
            Ok(Self::Other {
                name: other.to_owned(),
            })
        } else {
            Ok(Self::Transit {
                description: Some(s.to_owned()),
                progress_perc: None,
            })
        }
    }
}

/// Some sort of change that happened to a train.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct HistoryEvent {
    /// What happened.
    pub content: HistoryEventContent,
    /// When the thing happened.
    pub timestamp: DateTime<Utc>,
    /// Which trip ID the event is in.
    pub trip_id: usize,
}

/// Something that the processor (or rescuer, etc) did to the train state.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum ProcessorEvent {
    /// A train came into existence.
    New {
        train_id: String,
        line_code: String,
        trackernet_key: String,
    },
    /// A train had a new event happen to it.
    Update {
        train_id: String,
        line_code: String,
        event: HistoryEvent,
    },
    /// A train got archived.
    Archive { train_id: String },
    /// A train got 'rescued', linking two trains with identifiers.
    /// The `target_id` is no more and all of its events have been folded into `train_id`.
    Rescue { train_id: String, target_id: String },
}

/// The contents of a sort of change that happened to a train.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum HistoryEventContent {
    /// The train moved a bit, while staying in its current `Location`.
    Transit {
        /// The track code the train moved to.
        track_code: String,
        /// The description for the train's new location, if it changed.
        description: Option<String>,
        /// The new model point results for the train's current location, if they changed.
        model_point: Option<HashSet<String>>,
        /// As a percentage, how much progress has been made towards the next location.
        ///
        /// If unset, the current value is cleared. Although it may be present, it should be
        /// ignored unless the current location kind is `Transit`.
        progress_perc: Option<f32>,
    },
    /// The train moved a bit, and is now considered to be in a different `Location`.
    NewLocation {
        /// The new location.
        new_location: Location,
        /// Whether to remove a future location from the front of the list.
        advance_future: bool,
    },
    /// The train started a new trip.
    ///
    /// This "closes out" the current trip, writing a `Trip` record to the train's list of trips,
    /// and starts a new one; future events will have their `trip_id` incremented.
    NewTrip,
    /// The train's timetable identity changed.
    IdentityChange {
        /// The new train number.
        wtt_id: Option<u16>,
        /// The new trip number.
        wtt_trip: Option<u16>,
    },
    /// The leading car number of the train changed.
    CarChange {
        /// The new leading car number.
        leading_car_no: Option<String>,
    },
    /// The train's destination, or route to the destination, changed.
    DestinationChange {
        /// The new destination.
        destination: Destination,
        /// Whether this destination change is something a user would want to know about.
        ///
        /// For example, a change in platform number is probably not noteworthy on the Tube.
        minor: bool,
    },
    /// The train's route to the destination changed.
    RouteChange {
        /// A set of future predicted locations for this train (empty if the train is at its
        /// destination, or we have no idea where it's going)
        future_locations: Vec<FutureLocation>,
    },
    /// The train went in service.
    InService,
    /// The train went out of service.
    OutOfService,
    /// The train changed its branch.
    BranchChange { branch: Option<String> },
    /// The train changed its Trackernet identifier.
    Rescue { new_key: String },
}

impl HistoryEventContent {
    /// Whether this event should be added to the train's event log.
    pub fn is_recordable(&self) -> bool {
        match self {
            // Location-related events already get recorded in the locations list.
            Self::Transit { .. } => false,
            Self::NewLocation { .. } => false,
            // A change of route updates the future locations list, and isn't really noteworthy
            // since the real route will soon be discovered.
            Self::RouteChange { .. } => false,
            _ => true,
        }
    }
    pub fn with_time_and_trip(self, timestamp: DateTime<Utc>, trip_id: usize) -> HistoryEvent {
        HistoryEvent {
            timestamp,
            trip_id,
            content: self,
        }
    }
}

#[cfg(test)]
mod test {
    use crate::processor::LocationKind;

    #[test]
    fn location_parsing() {
        assert_eq!(
            "At Paddington Platform 6".parse::<LocationKind>().unwrap(),
            LocationKind::Station {
                name: "Paddington".into(),
                platform: Some("6".into())
            }
        );
        assert_eq!(
            "At Random Other Zone /A".parse::<LocationKind>().unwrap(),
            LocationKind::Other {
                name: "Random Other Zone".into(),
            }
        );
        assert_eq!(
            "At Random Other Zone - TA1234"
                .parse::<LocationKind>()
                .unwrap(),
            LocationKind::Other {
                name: "Random Other Zone".into(),
            }
        );
        assert_eq!(
            "At Random Other Zone".parse::<LocationKind>().unwrap(),
            LocationKind::Other {
                name: "Random Other Zone".into(),
            }
        );
        assert_eq!(
            "Between Euston and Mornington Crescent"
                .parse::<LocationKind>()
                .unwrap(),
            LocationKind::Transit {
                description: Some("Between Euston and Mornington Crescent".into()),
                progress_perc: None
            }
        );
    }
}
