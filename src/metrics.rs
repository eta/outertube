//! Prometheus metrics.

use lazy_static::lazy_static;
use prometheus::{
    register_gauge, register_gauge_vec, register_histogram, register_int_counter,
    register_int_counter_vec, Gauge, GaugeVec, Histogram, IntCounter, IntCounterVec,
};

lazy_static! {
    pub static ref SCRAPES_COUNTER: IntCounter = register_int_counter!(
        "trackernet_scrapes",
        "Number of completed trackernet requests"
    )
    .unwrap();
    pub static ref FAILED_SCRAPES_COUNTER: IntCounterVec = register_int_counter_vec!(
        "trackernet_failed_scrapes",
        "Number of failed trackernet requests",
        &["reason"]
    )
    .unwrap();
    pub static ref SCRAPE_TIME_HISTOGRAM: Histogram = register_histogram!(
        "trackernet_response_time",
        "Time taken by trackernet to send a response"
    )
    .unwrap();
    pub static ref DELAYED_SCRAPES_COUNTER: IntCounter = register_int_counter!(
        "trackernet_delayed_scrapes",
        "Number of times a trackernet scraper task didn't keep up",
    )
    .unwrap();
    pub static ref FAILED_SAVES_COUNTER: IntCounter = register_int_counter!(
        "outertube_data_save_failed",
        "Number of times outertube failed to write its data files"
    )
    .unwrap();
    pub static ref SAVE_SERIALIZATION_TIME: Gauge = register_gauge!(
        "outertube_data_serialization_time_ms",
        "Time in milliseconds spent serializing train data for save"
    )
    .unwrap();
    pub static ref SAVE_IO_TIME: Gauge = register_gauge!(
        "outertube_data_io_time_ms",
        "Time in milliseconds spent writing train data to disk"
    )
    .unwrap();
    pub static ref TRAINS_COUNT: Gauge = register_gauge!(
        "outertube_trains_count",
        "Number of trains in outertube's database"
    )
    .unwrap();
    pub static ref FIREHOSE_CONNECTIONS_COUNT: Gauge = register_gauge!(
        "outertube_firehose_connections",
        "Number of firehose connections open"
    )
    .unwrap();
    pub static ref ROUTE_STREAMER_COUNT: GaugeVec = register_gauge_vec!(
        "outertube_route_streamer_connections",
        "Number of route streaming connections open",
        &["line", "route"]
    )
    .unwrap();
    pub static ref ROUTE_STREAMER_UPDATE_COUNTER: IntCounterVec = register_int_counter_vec!(
        "outertube_route_streamer_updates",
        "Number of updates generated for each route",
        &["line", "route"]
    )
    .unwrap();
    pub static ref HTTP_REQUESTS_COUNTER: IntCounterVec = register_int_counter_vec!(
        "outertube_http_responses",
        "HTTP responses by status code",
        &["status"]
    )
    .unwrap();
    pub static ref FAILED_ROUTING_COUNTER: IntCounterVec = register_int_counter_vec!(
        "outertube_routing_failed",
        "Pairs of model points where outertube failed to find a route",
        &["line", "from", "to"]
    )
    .unwrap();
    pub static ref ACCURATE_ROUTE_COUNTER: IntCounterVec = register_int_counter_vec!(
        "outertube_route_prediction_successful",
        "Times when outertube correctly predicted the next location, by line code",
        &["line"]
    )
    .unwrap();
    pub static ref ROUTE_REGAIN_COUNTER: IntCounterVec = register_int_counter_vec!(
        "outertube_route_prediction_regained",
        "Times when outertube picked a route back up, by line code and track code",
        &["line", "track_code"]
    )
    .unwrap();
    pub static ref REROUTE_COUNTER: IntCounterVec = register_int_counter_vec!(
        "outertube_route_prediction_rerouted",
        "Times when outertube failed to predict the next location, by line code and track code",
        &["line", "track_code"]
    )
    .unwrap();
    pub static ref LOST_ROUTE_COUNTER: IntCounterVec = register_int_counter_vec!(
        "outertube_route_prediction_lost",
        "Times when outertube lost routing information, by line code and track code",
        &["line", "track_code"]
    )
    .unwrap();
}
