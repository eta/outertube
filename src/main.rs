use crate::clickhouse::ClickhouseClient;
use crate::config::Config;
use crate::processor::Processor;
use crate::scraper::{Scraper, STATIONS};
use rand::prelude::SliceRandom;
use slog::{info, o, trace, Drain};
#[cfg(target_os = "linux")]
use slog_journald::JournaldDrain;
use std::io::IsTerminal;
use std::sync::Arc;
use std::time::Instant;
use std::{fs, io};
use storage::TrainData;

mod clickhouse;
mod config;
mod metrics;
mod model;
mod processor;
mod rescuer;
mod routes;
mod scraper;
mod storage;
mod trackernet;
mod web;

fn main() -> anyhow::Result<()> {
    let config_path = std::env::args()
        .nth(1)
        .unwrap_or_else(|| "./config.toml".to_string());
    eprintln!("loading {config_path}");
    let config_str = fs::read_to_string(&config_path)?;
    let config: Config = toml::from_str(&config_str)?;
    let decorator = slog_term::TermDecorator::new().build();
    let drain = if cfg!(not(target_os = "linux"))
        || (io::stdout().is_terminal() && !config.force_journald)
    {
        eprintln!("running interactively");
        let drain = slog_term::FullFormat::new(decorator).build().fuse();
        slog_async::Async::new(drain).build().fuse()
    } else {
        eprintln!("sending logs to systemd-journald");
        #[cfg(not(target_os = "linux"))]
        unreachable!();
        #[cfg(target_os = "linux")]
        slog_async::Async::new(JournaldDrain.ignore_res())
            .build()
            .fuse()
    };

    let log = slog::Logger::root(drain, o!());
    info!(log, "hello from outertube/{}", env!("CARGO_PKG_VERSION"));

    let (mut events_tx, events_rx) = async_broadcast::broadcast(512);
    events_tx.set_await_active(false);
    events_tx.set_overflow(true);
    let events_rx = events_rx.deactivate();

    let clickhouse = if let Some(ch) = config.clickhouse {
        info!(log, "using clickhouse server at {}", ch.base_url);
        Some(ClickhouseClient::new(ch)?)
    } else {
        None
    };

    info!(log, "reading train data from {}", config.storage.path);
    let train_data = TrainData::new_from_disk(&config.storage.path, events_rx, clickhouse)?;
    Arc::clone(&train_data).start_writing_task(config.storage.clone(), log.clone())?;

    info!(log, "starting http server on {}", config.http.listen);
    web::start_web(config.http, Arc::clone(&train_data), log.clone());
    info!(log, "starting rescuer loop");
    rescuer::start_rescuer_and_archiver(Arc::clone(&train_data), events_tx.clone(), log.clone());
    info!(log, "starting scraper");
    let mut stations = Vec::from(STATIONS);
    stations.shuffle(&mut rand::thread_rng());
    let scraper = Scraper::new(stations, config.trackernet, &log)?;
    let mut processor = Processor::new(train_data, config.processor, events_tx)?;
    smol::block_on(async {
        let start = Instant::now();
        loop {
            let observation = scraper.next().await;
            trace!(
                log,
                "+{:.02}s: {}/{} trains from {}-{}",
                (Instant::now() - start).as_secs_f64(),
                observation.trains.len(),
                observation.minimal_trains.len(),
                observation.line_code,
                observation.station_code
            );
            processor.process(observation, &log);
        }
    });
    Ok(())
}
