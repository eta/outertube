//! Scraping Trackernet observations by polling station departure boards.

use crate::config::TrackernetConfig;
use crate::metrics;
use crate::trackernet::TrackernetObservation;
use anyhow::{bail, Context};
use either::Either;
use futures::future::FutureExt;
use isahc::config::Configurable;
use isahc::http::StatusCode;
use isahc::HttpClient;
use slog::{o, warn, Logger};
use smol::channel::{Receiver, Sender, TrySendError};
use smol::future::FutureExt as SmolFutureExt;
use smol::io::AsyncReadExt;
use smol::{LocalExecutor, Timer};
use std::borrow::Cow;
use std::fmt;
use std::fmt::Formatter;
use std::rc::Rc;
use std::str::FromStr;
use std::time::{Duration, Instant};

/// A list of stations to poll.
pub const STATIONS: [Station; 141] = [
    // District line
    Station::new("D", "UPM"), // Upminster (terminus)
    Station::new("D", "BKG"), // Barking (between UPM and THL)
    Station::new("D", "THL"), // Tower Hill (bay platform)
    Station::new("D", "ECT"), // Earl's Court
    Station::new("D", "EBY"), // Ealing Broadway
    Station::new("D", "OLY"), // Kensington (Olympia)
    Station::new("D", "HST"), // High St Kensington
    Station::new("D", "FBY"), // Fulham Broadway
    Station::new("D", "HMD"), // Hammersmith
    Station::new("D", "ALE"), // Aldgate East
    Station::new("D", "ECM"), // Ealing Common
    Station::new("D", "ACT"), // Acton Town
    Station::new("D", "GUN"), // Gunnersbury
    Station::new("D", "SKN"), // South Kensington
    Station::new("D", "EMB"), // Embankment
    Station::new("D", "WHM"), // West Ham
    Station::new("D", "PLW"), // Plaistow
    Station::new("D", "EHM"), // East Ham
    Station::new("D", "UPY"), // Upney
    Station::new("D", "DGE"), // Dagenham East
    Station::new("D", "ERD"), // Edgware Road
    Station::new("D", "WKN"), // West Kensington
    Station::new("D", "WDN"), // Wimbledon
    Station::new("D", "RMD"), // Richmond
    // Victoria line
    Station::new("V", "BRX"), // Brixton
    Station::new("V", "WAL"), // Walthamstow Central
    Station::new("V", "SVS"), // Seven Sisters
    Station::new("V", "HBY"), // Highbury & Islington
    Station::new("V", "WST"), // Warren Street
    Station::new("V", "KXX"), // King's Cross St Pancras
    Station::new("V", "VIC"), // Victoria
    // Jubilee line
    Station::new("J", "WHD"), // West Hampstead
    Station::new("J", "WPK"), // Wembley Park
    Station::new("J", "STA"), // Stanmore
    Station::new("J", "SFD"), // Stratford
    Station::new("J", "WLO"), // Waterloo
    Station::new("J", "NGW"), // North Greenwich
    Station::new("J", "CPK"), // Canons Park
    Station::new("J", "NEA"), // Neasden
    Station::new("J", "FRD"), // Finchley Road
    Station::new("J", "GPK"), // Green Park
    Station::new("J", "WLO"), // Waterloo
    Station::new("J", "LON"), // London Bridge
    Station::new("J", "CWF"), // Canary Wharf
    Station::new("J", "WHM"), // West Ham
    // Northern line
    Station::new("N", "MOR"), // Morden
    Station::new("N", "EUS"), // Euston
    Station::new("N", "HBT"), // High Barnet
    Station::new("N", "KEN"), // Kennington
    Station::new("N", "MHE"), // Mill Hill East
    Station::new("N", "EDG"), // Edgware
    Station::new("N", "CTN"), // Camden Town
    Station::new("N", "ARC"), // Archway
    Station::new("N", "FYC"), // Finchley Central
    Station::new("N", "EFY"), // East Finchley
    Station::new("N", "MCR"), // Mornington Crescent
    Station::new("N", "COL"), // Colindale
    Station::new("N", "HMP"), // Hampstead
    Station::new("N", "KXX"), // King's Cross St Pancras
    Station::new("N", "CHX"), // Charing Cross
    Station::new("N", "STK"), // Stockwell
    Station::new("N", "TBY"), // Tooting Broadway
    Station::new("N", "MGT"), // Moorgate
    //     Station::new("N", "BPS"), // Battersea Power Station

    // Piccadilly line
    Station::new("P", "ACT"), // Acton Town
    Station::new("P", "HRV"), // Heathrow Terminal 5
    Station::new("P", "CFS"), // Cockfosters
    Station::new("P", "UXB"), // Uxbridge
    Station::new("P", "RLN"), // Rayners Lane
    Station::new("P", "KXX"), // King's Cross St Pancras
    Station::new("P", "HMD"), // Hammersmith
    Station::new("P", "NFD"), // Northfields
    Station::new("P", "WGN"), // Wood Green
    Station::new("P", "GPK"), // Green Park
    Station::new("P", "HPC"), // Hyde Park Corner
    Station::new("P", "BOS"), // Boston Manor
    Station::new("P", "HNC"), // Hounslow Central
    Station::new("P", "HRC"), // Heathrow Terminals 123
    Station::new("P", "ECM"), // Ealing Common
    Station::new("P", "SHR"), // South Harrow
    Station::new("P", "RUI"), // Ruislip
    // Central line
    Station::new("C", "WRP"), // West Ruislip
    Station::new("C", "NHT"), // Northolt
    Station::new("C", "EBY"), // Ealing Broadway
    Station::new("C", "NAC"), // North Acton
    Station::new("C", "WCT"), // White City
    Station::new("C", "MAR"), // Marble Arch
    Station::new("C", "LYS"), // Leytonstone
    Station::new("C", "WFD"), // Woodford
    Station::new("C", "HAI"), // Hainault
    Station::new("C", "EPP"), // Epping
    Station::new("C", "NEP"), // Newbury Park
    Station::new("C", "LTN"), // Loughton
    Station::new("C", "RUG"), // Ruislip Gardens
    Station::new("C", "QWY"), // Queensway
    Station::new("C", "LST"), // Liverpool Street
    Station::new("C", "BNG"), // Bethnal Green
    Station::new("C", "GRH"), // Grange Hill
    Station::new("C", "DEB"), // Debden
    // Bakerloo line
    Station::new("B", "HAW"), // Harrow & Wealdstone
    Station::new("B", "SPK"), // Stonebridge Park
    Station::new("B", "ELE"), // Elephant & Castle
    Station::new("B", "QPK"), // Queen's Park
    Station::new("B", "WEM"), // Wembley Central
    Station::new("B", "WJN"), // Willesden Junction
    Station::new("B", "PAD"), // Paddington
    Station::new("B", "PIC"), // Piccadilly Circus
    Station::new("B", "WLO"), // Waterloo
    Station::new("B", "LAM"), // Lambeth North
    // Metropolitan line
    Station::new("M", "ALD"), // Aldgate
    Station::new("M", "AME"), // Amersham
    Station::new("M", "CLF"), // Chalfont & Latimer
    Station::new("M", "UXB"), // Uxbridge
    Station::new("M", "WAT"), // Watford
    Station::new("M", "HOH"), // Harrow-on-the-Hill
    Station::new("M", "BST"), // Baker Street
    Station::new("M", "RKY"), // Rickmansworth
    Station::new("M", "MGT"), // Moorgate
    Station::new("M", "KXX"), // King's Cross St Pancras
    Station::new("M", "FRD"), // Finchley Road
    Station::new("M", "WPK"), // Wembley Park
    Station::new("M", "RLN"), // Rayners Lane
    Station::new("M", "RUI"), // Ruislip
    Station::new("M", "HDN"), // Hillingdon
    Station::new("M", "NWD"), // Northwood
    // Hammersmith & Circle lines
    Station::new("H", "HMS"), // Hammersmith
    Station::new("H", "ERD"), // Edgware Road
    Station::new("H", "WMS"), // Westminster
    Station::new("H", "KXX"), // King's Cross St Pancras
    Station::new("H", "BKG"), // Barking
    Station::new("H", "PAD"), // Paddington
    Station::new("H", "BST"), // Baker Street
    Station::new("H", "SKN"), // South Kensington
    Station::new("H", "HST"), // High Street Kensington
    Station::new("H", "ALD"), // Aldgate
    Station::new("H", "ALE"), // Aldgate East
    Station::new("H", "WCL"), // Whitechapel
    Station::new("H", "WHM"), // West Ham
    Station::new("H", "PLW"), // Plaistow
    Station::new("H", "EHM"), // East Ham
    // Waterloo & City line
    Station::new("W", "WLO"), // Waterloo
    Station::new("W", "BNK"), // Bank
];

#[derive(Debug, Clone, PartialEq)]
pub struct Station {
    pub line_code: Cow<'static, str>,
    pub station_code: Cow<'static, str>,
}

impl fmt::Display for Station {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}-{}", self.line_code, self.station_code)
    }
}

impl Station {
    pub const fn new(line_code: &'static str, station_code: &'static str) -> Self {
        Self {
            line_code: Cow::Borrowed(line_code),
            station_code: Cow::Borrowed(station_code),
        }
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
struct ScrapeTiming {
    offset: Duration,
    cycle_time: Duration,
}

fn generate_scrape_timings(
    requests_per_min: u64,
    scrapers: u32,
) -> impl Iterator<Item = ScrapeTiming> {
    // The scheduling logic here is easy to get wrong, so let's have a diagram...
    // (Here, A, B and C are points in time at which we do a scrape.)
    //
    //   /--------- 1 minute ----------\
    //   A <-> B <-> C <-> A <-> B <-> C
    //      |
    //      - `inter_scrape_delay`
    //
    // If we assume scrapes take zero time, the minute should be evenly divided amongst the
    // waiting periods (<->) between the scrapes, `inter_scrape_delay`.
    //
    // 60sec / inter_scrape_delay = config.requests_per_min
    // => inter_scrape_delay = 60sec / config.requests_per_min
    let inter_scrape_delay = Duration::from_millis(60_000 / requests_per_min);

    // We make the scrapers keep to this schedule with an `offset` and a `delay_per_request`.
    // The scrapers wait the `offset` before starting their loops, which is just a multiple of
    // the `inter_scrape_delay`:
    //
    //   A <-> B <-> C <-> A <-> B <-> C
    //   A starts immediately
    //   ----- B waits 1 × i_s_d and then starts
    //   ----------- C waits 2 × i_s_d and then starts
    //
    // Then, the scrapers wait for their next turn:
    //
    //   A <-> B <-> C <-> A <-> B <-> C
    //   \----cycle_time---/
    //         \----cycle_time---/
    //               \----cycle_time---/
    //
    // The time they wait, `cycle_time`, is the count of scrapers multiplied by the `i_s_d`.
    let cycle_time = inter_scrape_delay * scrapers;

    (0..(scrapers as usize)).map(move |i| ScrapeTiming {
        cycle_time,
        offset: inter_scrape_delay * (i as u32),
    })
}

/// Scrapes from a configured list of stations on Trackernet.
pub struct Scraper {
    executor: LocalExecutor<'static>,
    rx: Receiver<TrackernetObservation>,
}

impl Scraper {
    pub fn new(
        stations: Vec<Station>,
        config: TrackernetConfig,
        logger: &Logger,
    ) -> anyhow::Result<Self> {
        let client = Rc::new(TrackernetClient::new(stations.len(), config.clone())?);
        // Make an executor to put all of our ScraperTasks on, and a channel to receive the results.
        let executor = LocalExecutor::new();
        // We should expect the channel to get cleared within 15 seconds, else something is
        // seriously backing up!
        let (tx, rx) = smol::channel::bounded(config.requests_per_min.div_ceil(4) as usize);

        let scrape_timings = generate_scrape_timings(config.requests_per_min, stations.len() as _);

        for (station, ScrapeTiming { offset, cycle_time }) in
            stations.into_iter().zip(scrape_timings)
        {
            let station_pretty = station.to_string();
            executor
                .spawn(
                    ScraperTask {
                        client: Rc::clone(&client),
                        tx: tx.clone(),
                        station,
                        offset,
                        cycle_time,
                        log: logger.new(o!("scraper" => station_pretty)),
                    }
                    .run(),
                )
                .detach(); // No need to keep the handles around, really
        }

        Ok(Self { executor, rx })
    }

    pub async fn next(&self) -> TrackernetObservation {
        loop {
            // We poll the channel before we poll the executor to exert backpressure on the scraper
            // tasks.
            if let Some(obs) = self
                .rx
                .recv()
                .map(|x| Some(x.expect("scraper tasks went away")))
                .or(self.executor.tick().map(|_| None))
                .await
            {
                return obs;
            }
        }
    }
}

struct ScraperTask {
    client: Rc<TrackernetClient>,
    tx: Sender<TrackernetObservation>,
    station: Station,
    offset: Duration,
    cycle_time: Duration,
    log: Logger,
}

impl ScraperTask {
    async fn run(self) {
        // Wait our start offset out.
        Timer::after(self.offset).await;
        // Scrape in a loop until our receiver hangs up.
        loop {
            let now = Instant::now();
            let mut next_scrape = now + self.cycle_time;

            match self
                .client
                .fetch_station(&self.station.line_code, &self.station.station_code)
                .map(Either::Left)
                .or(Timer::at(next_scrape).map(|_| Either::Right(())))
                .await
            {
                Either::Left(Ok(obs)) => {
                    let time = Instant::now()
                        .checked_duration_since(now)
                        .map(|d| d.as_secs_f64())
                        .unwrap_or(f64::MAX);
                    metrics::SCRAPES_COUNTER.inc();
                    metrics::SCRAPE_TIME_HISTOGRAM.observe(time);

                    match self.tx.try_send(obs) {
                        Ok(_) => {}
                        Err(TrySendError::Full(obs)) => {
                            warn!(self.log, "scraper experiencing backpressure!");
                            let _ = self.tx.send(obs).await;
                        }
                        // This isn't possible (our executor will go away first).
                        Err(TrySendError::Closed(_)) => unreachable!(),
                    }
                }
                Either::Left(Err(e)) => {
                    // FIXME(eta): exponential backoff should happen here
                    warn!(self.log, "scraper errored: {e:?}");
                    metrics::FAILED_SCRAPES_COUNTER
                        .with_label_values(&["errored"])
                        .inc();
                }
                Either::Right(()) => {
                    warn!(self.log, "scraper exceeded deadline");
                    metrics::FAILED_SCRAPES_COUNTER
                        .with_label_values(&["timeout"])
                        .inc();
                }
            }

            let now = Instant::now();
            if now > next_scrape {
                warn!(
                    self.log,
                    "scraper couldn't keep up! behind by {:.01}sec",
                    (now - next_scrape).as_secs_f32()
                );
                metrics::DELAYED_SCRAPES_COUNTER.inc();
                // Fast forward so we're back on schedule.
                // (This could be done more efficiently, but whatever. This way works.)
                while now > next_scrape {
                    next_scrape += self.cycle_time;
                }
            }
            Timer::at(next_scrape).await;
        }
    }
}

/// Obtains observations from Trackernet.
pub struct TrackernetClient {
    http: HttpClient,
    config: TrackernetConfig,
}

impl TrackernetClient {
    pub fn new(stations_len: usize, config: TrackernetConfig) -> anyhow::Result<Self> {
        let http = HttpClient::builder()
            .connection_cache_size(stations_len)
            .default_header(
                "User-Agent",
                concat!(
                    "outertube/",
                    env!("CARGO_PKG_VERSION"),
                    " (contact me at intertube@eta.st)"
                ),
            )
            .default_header("app_key", config.app_key.clone())
            .timeout(Duration::from_secs(config.request_timeout_sec))
            .build()?;

        Ok(Self { http, config })
    }

    pub async fn fetch_station(
        &self,
        line: &str,
        code: &str,
    ) -> anyhow::Result<TrackernetObservation> {
        let mut url = self.config.base_url.clone();

        url.path_segments_mut()
            .unwrap()
            .extend(["PredictionDetailed", line, code]);

        let mut response = self
            .http
            .get_async(url.as_str())
            .await
            .context("trackernet http request")?;

        let mut body = String::new();

        response
            .body_mut()
            .read_to_string(&mut body)
            .await
            .context("reading trackernet http response body")?;

        if response.status() != StatusCode::OK {
            bail!(
                "trackernet returned {} requesting {url}: {body}",
                response.status()
            );
        }

        let ret =
            TrackernetObservation::from_str(&body).context("parsing trackernet response body")?;

        if ret.station_code != code || ret.line_code != line {
            bail!(
                "trackernet observation is lying: requested {}-{} but got {}-{}",
                line,
                code,
                ret.line_code,
                ret.station_code
            );
        }

        Ok(ret)
    }
}

#[cfg(test)]
mod tests {
    use crate::scraper::{generate_scrape_timings, ScrapeTiming};
    use std::time::Duration;

    #[test]
    fn test_scrape_timings() {
        let timings = generate_scrape_timings(10, 3).collect::<Vec<_>>();
        let cycle_time = Duration::from_secs(18);

        assert_eq!(
            timings,
            vec![
                ScrapeTiming {
                    offset: Duration::from_secs(0),
                    cycle_time
                },
                ScrapeTiming {
                    offset: Duration::from_secs(6),
                    cycle_time
                },
                ScrapeTiming {
                    offset: Duration::from_secs(12),
                    cycle_time
                },
            ]
        );
    }
}
