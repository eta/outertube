//! Deserializing trackernet trains from the raw XML.

use chrono::{DateTime, NaiveDateTime};
use chrono_tz::{Europe, Tz};
use serde::{Deserialize, Deserializer, Serialize};
use std::str::FromStr;

#[derive(Clone, PartialEq, Debug)]
pub struct TrackernetObservation {
    pub timestamp: DateTime<Tz>,
    pub line_code: String,
    pub line_name: String,
    pub station_code: String,
    pub station_name: String,
    pub trains: Vec<TrackernetTrain>,
    pub minimal_trains: Vec<MinimalTrackernetTrain>,
}

impl FromStr for TrackernetObservation {
    type Err = quick_xml::de::DeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let root: Root = quick_xml::de::from_str(s)?;

        let mut trains = vec![];
        let mut minimal_trains = vec![];

        for platform in root.station.platforms {
            for train in platform.trains {
                match train {
                    ParsedTrackernetTrain::Full(t) => trains.push(t),
                    ParsedTrackernetTrain::Minimal(t) => minimal_trains.push(t),
                }
            }
        }

        Ok(Self {
            timestamp: root.when_created,
            line_code: root.line_code,
            line_name: root.line_name,
            station_code: root.station.code,
            station_name: root.station.name,
            trains,
            minimal_trains,
        })
    }
}

#[derive(Deserialize, Debug)]
struct Root {
    #[serde(rename = "WhenCreated")]
    #[serde(deserialize_with = "deserialize_when_created")]
    when_created: DateTime<Tz>,
    #[serde(rename = "Line")]
    line_code: String,
    #[serde(rename = "LineName")]
    line_name: String,
    #[serde(rename = "S")]
    station: S,
}

fn deserialize_when_created<'de, D>(deserializer: D) -> Result<DateTime<Tz>, D::Error>
where
    D: Deserializer<'de>,
{
    use serde::de::Error;

    let text = String::deserialize(deserializer)?;

    // "13 Nov 2024 18:27:16"
    let naive =
        NaiveDateTime::parse_from_str(&text, "%d %b %Y %H:%M:%S").map_err(D::Error::custom)?;

    naive
        .and_local_timezone(Europe::London)
        .single()
        .ok_or(D::Error::custom(format!(
            "failed to map naive time {naive}"
        )))
}

#[derive(Deserialize, Debug)]
#[allow(dead_code)]
struct S {
    #[serde(rename = "@Code")]
    code: String,
    #[serde(rename = "@Mess")]
    message: String,
    #[serde(rename = "@N")]
    name: String,
    #[serde(rename = "@CurTime")]
    current_time: String,
    #[serde(rename = "P")]
    platforms: Vec<P>,
}

#[derive(Deserialize, Debug)]
#[allow(dead_code)]
struct P {
    #[serde(rename = "@Num")]
    number: String,
    #[serde(rename = "@N")]
    name: String,
    #[serde(rename = "@TrackCode")]
    track_code: String,
    #[serde(rename = "@NextTrain")]
    next_train: bool,
    #[serde(rename = "T")]
    #[serde(default)]
    trains: Vec<ParsedTrackernetTrain>,
}

#[derive(Debug)]
enum ParsedTrackernetTrain {
    Full(TrackernetTrain),
    Minimal(MinimalTrackernetTrain),
}

impl<'de> Deserialize<'de> for ParsedTrackernetTrain {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        use serde::de::Error;

        let raw = T::deserialize(deserializer)?;

        let raw_debug = format!("{raw:?}");
        let missing_field = |name: &'static str| {
            D::Error::custom(format!(
                "couldn't deserialize <T/> element: missing field '{name}' in {raw_debug}"
            ))
        };

        let (set_no, headcode) = match raw.set_no {
            None => (None, None),
            // Collapse empty string to None.
            Some(s) if s.is_empty() => (None, None),
            Some(s) => match u16::from_str(&s) {
                // Collapse 0 to None.
                Ok(0) => (None, None),
                Ok(n) => (Some(n), None),
                // Preserve the not-actually-a-set-number in the headcode field.
                Err(_) => (None, Some(s)),
            },
        };

        let ret = if raw.train_id.is_some() {
            ParsedTrackernetTrain::Full(TrackernetTrain {
                train_id: raw.train_id.ok_or_else(|| missing_field("train_id"))?,
                lcid: raw.lcid,
                set_no,
                trip_no: raw.trip_no,
                headcode,
                seconds_to: raw.seconds_to,
                time_to: raw.time_to,
                location_desc: raw.location_desc,
                overridden_location_desc: None,
                destination_desc: raw
                    .destination_desc
                    .ok_or_else(|| missing_field("destination_desc"))?,
                track_code: raw.track_code.ok_or_else(|| missing_field("track_code"))?,
                line_code: raw.line_code.ok_or_else(|| missing_field("line_code"))?,
                leading_car_no: raw
                    .leading_car_no
                    .ok_or_else(|| missing_field("leading_car_no"))?,
                input_dest: raw.input_dest,
                dest_code: raw.dest_code,
                order: raw.order,
                arrival_time: raw.arrival_time,
                depart_time: raw.depart_time,
                depart_interval: raw.depart_interval,
                departed: raw.departed,
                direction: raw.direction,
                is_stalled: raw.is_stalled,
            })
        } else {
            ParsedTrackernetTrain::Minimal(MinimalTrackernetTrain {
                set_no,
                trip_no: None,
                headcode,
                seconds_to: None,
                time_to: raw.time_to,
                location_desc: raw.location_desc,
                destination_desc: raw
                    .destination_desc
                    .ok_or_else(|| missing_field("destination_desc"))?,
                arrival_time: None,
                depart_time: None,
                depart_interval: None,
                departed: None,
                direction: None,
                is_stalled: None,
            })
        };
        Ok(ret)
    }
}

#[derive(Deserialize, Debug)]
struct T {
    #[serde(rename = "@TrainId")]
    #[serde(default)]
    train_id: Option<String>,
    #[serde(rename = "@LCID")]
    #[serde(deserialize_with = "deserialize_zeroable_numeric")]
    #[serde(default)]
    lcid: Option<u16>,
    #[serde(rename = "@SetNo")]
    #[serde(default)]
    set_no: Option<String>,
    #[serde(rename = "@TripNo")]
    #[serde(deserialize_with = "deserialize_zeroable_numeric")]
    #[serde(default)]
    trip_no: Option<u16>,
    #[serde(rename = "@SecondsTo")]
    #[serde(default)]
    seconds_to: Option<u32>,
    #[serde(rename = "@TimeTo")]
    #[serde(default)]
    time_to: Option<String>,
    #[serde(rename = "@Location")]
    #[serde(default)]
    location_desc: Option<String>,
    #[serde(rename = "@Destination")]
    #[serde(default)]
    destination_desc: Option<String>,
    #[serde(rename = "@InputDest")]
    #[serde(default)]
    input_dest: Option<String>,
    #[serde(rename = "@DestCode")]
    #[serde(default)]
    dest_code: Option<String>,
    #[serde(rename = "@Order")]
    #[serde(default)]
    order: Option<String>,
    #[serde(rename = "@ArrivalTime")]
    #[serde(default)]
    arrival_time: Option<String>,
    #[serde(rename = "@DepartTime")]
    #[serde(default)]
    depart_time: Option<String>,
    #[serde(rename = "@DepartInterval")]
    #[serde(default)]
    depart_interval: Option<String>,
    #[serde(rename = "@Departed")]
    #[serde(default)]
    departed: Option<String>,
    #[serde(rename = "@Direction")]
    #[serde(default)]
    direction: Option<String>,
    #[serde(rename = "@IsStalled")]
    #[serde(default)]
    is_stalled: Option<String>,
    #[serde(rename = "@TrackCode")]
    #[serde(default)]
    track_code: Option<String>,
    #[serde(rename = "@LN")]
    #[serde(default)]
    line_code: Option<String>,
    #[serde(rename = "@LeadingCarNo")]
    #[serde(default)]
    leading_car_no: Option<String>,
}

fn deserialize_zeroable_numeric<'de, D>(deserializer: D) -> Result<Option<u16>, D::Error>
where
    D: Deserializer<'de>,
{
    use serde::de::Error;

    match Option::<String>::deserialize(deserializer)? {
        None => Ok(None),
        Some(n) if n.is_empty() => Ok(None),
        Some(n) => match u16::from_str(&n) {
            Ok(0) => Ok(None),
            Ok(n) => Ok(Some(n)),
            Err(e) => Err(Error::custom(format!(
                "invalid numeric field value '{n}': {e}"
            ))),
        },
    }
}

/// A Trackernet train object obtained from a station departure board (`<T/>` element in Trackernet XML).
#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct TrackernetTrain {
    pub train_id: String,
    pub lcid: Option<u16>,
    pub set_no: Option<u16>,
    pub trip_no: Option<u16>,
    /// Sometimes the `set_no` is actually an NR headcode; that gets put here.
    pub headcode: Option<String>,
    pub location_desc: Option<String>,
    // Populated by the processor in the case of an override
    #[serde(default)]
    pub overridden_location_desc: Option<String>,
    pub destination_desc: String,
    pub track_code: String,
    pub line_code: String,
    pub leading_car_no: String,
    pub input_dest: Option<String>,
    pub dest_code: Option<String>,
    pub seconds_to: Option<u32>,
    //
    // Fields we never use:
    //
    pub time_to: Option<String>,
    pub order: Option<String>,
    pub arrival_time: Option<String>,
    pub depart_time: Option<String>,
    pub depart_interval: Option<String>,
    pub departed: Option<String>,
    pub direction: Option<String>,
    pub is_stalled: Option<String>,
}

impl TrackernetTrain {
    /// Extract our best guess at a 'unique train ID' for this observation.
    pub fn trackernet_key(&self) -> String {
        if let Some(lcid) = self.lcid {
            return lcid.to_string();
        }
        // The Metropolitan line often has the same train under two different train ids, so prefer
        // the set number.
        if self.line_code == "M" {
            if let Some(set) = self.set_no {
                return format!("set_{set}");
            }
        }
        format!("tid_{}", self.train_id)
    }
}

/// A minimal `TrackernetTrain` that's missing half the fields.
///
/// Trackernet returns these sometimes for unexplained reasons (usually either trains on another
/// line entirely, or trains that are being located purely using their Connect radio).
#[derive(Clone, PartialEq, Debug)]
pub struct MinimalTrackernetTrain {
    pub set_no: Option<u16>,
    pub trip_no: Option<u16>,
    /// Sometimes the `set_no` is actually an NR headcode; that gets put here.
    pub headcode: Option<String>,
    pub seconds_to: Option<u32>,
    pub time_to: Option<String>,
    pub location_desc: Option<String>,
    pub destination_desc: String,
    pub arrival_time: Option<String>,
    pub depart_time: Option<String>,
    pub depart_interval: Option<String>,
    pub departed: Option<String>,
    pub direction: Option<String>,
    pub is_stalled: Option<String>,
}

#[cfg(test)]
mod tests {
    use crate::trackernet::TrackernetObservation;
    use std::str::FromStr;

    #[test]
    fn westminster_jubilee() {
        let observations = TrackernetObservation::from_str(include_str!(
            "../test/2024-11-13-westminster-jubilee.xml"
        ))
        .unwrap();

        eprintln!("{observations:#?}");

        assert!(observations.minimal_trains.is_empty());
    }

    #[test]
    fn westminster_district() {
        let observations = TrackernetObservation::from_str(include_str!(
            "../test/2024-11-13-westminster-district.xml"
        ))
        .unwrap();

        eprintln!("{observations:#?}");
    }

    #[test]
    fn earls_court_district() {
        let observations = TrackernetObservation::from_str(include_str!(
            "../test/2024-11-14-earls-court-district.xml"
        ))
        .unwrap();

        eprintln!("{observations:#?}");
    }
}
