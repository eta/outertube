//! Storing train data.

use crate::clickhouse::ClickhouseClient;
use crate::config::StorageConfig;
use crate::metrics::{FAILED_SAVES_COUNTER, SAVE_IO_TIME, SAVE_SERIALIZATION_TIME, TRAINS_COUNT};
use crate::processor::{ProcessorEvent, Train};
use crate::trackernet::TrackernetTrain;
use anyhow::Context;
use async_broadcast::InactiveReceiver;
use async_signal::{Signal, Signals};
use chrono::{DateTime, Utc};
use flate2::bufread::GzDecoder;
use flate2::write::GzEncoder;
use flate2::Compression;
use serde::{Deserialize, Serialize};
use slog::{error, info, Logger};
use smol::stream::StreamExt;
use smol::Timer;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufReader, BufWriter, Write};
use std::sync::{Arc, RwLock};
use std::task::Poll;
use std::time::{Duration, Instant};
use std::{future, io, thread};

/// A very basic shared data store for train data.
pub struct TrainData {
    pub inner: RwLock<TrainDataInner>,
    pub events_rx: InactiveReceiver<ProcessorEvent>,
    pub clickhouse: Option<ClickhouseClient>,
}

#[derive(Deserialize, Serialize)]
pub struct TrainDataInner {
    /// A map from a train's "identity key" (a tuple of line code and trackernet key) to train ids.
    #[serde(skip)]
    pub key_map: HashMap<(String, String), String>,
    /// Trains, indexed by their unique id.
    pub trains: HashMap<String, Train>,
    /// A map of train unique ids to the raw trackernet observations (with timestamps).
    pub raw_observations: HashMap<String, Vec<(DateTime<Utc>, TrackernetTrain)>>,
}

impl TrainData {
    pub fn new(
        events_rx: InactiveReceiver<ProcessorEvent>,
        clickhouse: Option<ClickhouseClient>,
    ) -> Arc<Self> {
        TRAINS_COUNT.set(0.0f64);
        Arc::new(Self {
            inner: RwLock::new(TrainDataInner {
                key_map: HashMap::new(),
                trains: HashMap::new(),
                raw_observations: HashMap::new(),
            }),
            events_rx,
            clickhouse,
        })
    }
    pub fn new_from_disk(
        path: &str,
        events_rx: InactiveReceiver<ProcessorEvent>,
        clickhouse: Option<ClickhouseClient>,
    ) -> anyhow::Result<Arc<Self>> {
        let file = match File::open(path) {
            Ok(f) => f,
            Err(e) if e.kind() == io::ErrorKind::NotFound => {
                return Ok(Self::new(events_rx, clickhouse));
            }
            Err(e) => return Err(anyhow::Error::new(e).context("failed to open data file")),
        };
        let bufreader = BufReader::new(file);
        let decoder = GzDecoder::new(bufreader);
        let mut inner: TrainDataInner =
            bincode::deserialize_from(decoder).context("failed to deserialize train data")?;
        for (id, train) in inner.trains.iter() {
            inner.key_map.insert(
                (
                    train.line_code().to_owned(),
                    train.trackernet_key().to_owned(),
                ),
                id.clone(),
            );
        }
        TRAINS_COUNT.set(inner.trains.len() as f64);
        Ok(Arc::new(Self {
            inner: RwLock::new(inner),
            events_rx,
            clickhouse,
        }))
    }
    pub fn start_writing_task(
        self: Arc<Self>,
        config: StorageConfig,
        log: Logger,
    ) -> anyhow::Result<()> {
        let mut signals = Signals::new([Signal::Term, Signal::Int, Signal::Quit])
            .context("failed to start signals listener")?;

        let mut timer = Timer::interval(Duration::from_secs(config.save_interval_sec));

        thread::spawn(move || {
            smol::block_on(future::poll_fn(|cx| {
                if timer.poll_next(cx).is_ready() {
                    self.log_write_to_disk("regular", &log, &config.path);
                }
                if let Poll::Ready(sig) = signals.poll_next(cx) {
                    let sig = sig
                        .expect("signals iterator dried up")
                        .expect("signals iterator error");
                    info!(log, "Received signal {sig:?}, quitting.");
                    self.log_write_to_disk("signal", &log, &config.path);
                    std::process::exit(0);
                }

                Poll::<()>::Pending
            }));
        });
        Ok(())
    }
    fn log_write_to_disk(&self, reason: &str, log: &Logger, path: &str) {
        info!(log, "Saving trains to disk ({reason})...");
        if let Err(e) = self.write_to_disk(path, log) {
            error!(log, "Failed to save train data: {e:?}");
            FAILED_SAVES_COUNTER.inc();
        }
    }

    pub fn write_to_disk(&self, path: &str, log: &Logger) -> anyhow::Result<()> {
        let temp_path = format!("{path}-temp");
        let writer = File::create(&temp_path).context("failed to create temp file")?;
        let mut writer = GzEncoder::new(BufWriter::new(writer), Compression::fast());
        let (time, serialized) = {
            let inner = self.inner.read().unwrap();
            let start = Instant::now();
            let ser = bincode::serialize(&*inner).context("failed serializing train data")?;
            (Instant::now() - start, ser)
        };
        let size_bytes = serialized.len();
        let start = Instant::now();
        writer
            .write_all(&serialized)
            .context("writing out buffer failed")?;
        writer
            .finish()
            .context("decoder finish failed")?
            .into_inner()
            .context("bufreader flush failed")?
            .sync_all()
            .context("fsync failed")?;
        std::fs::rename(&temp_path, path).context("rename failed")?;
        let fs_time = Instant::now() - start;
        SAVE_SERIALIZATION_TIME.set(time.as_millis() as f64);
        SAVE_IO_TIME.set(fs_time.as_millis() as f64);
        info!(
            log,
            "Saved {} MiB of train data ({}ms serialization, {}ms I/O).",
            size_bytes / (1024 * 1024),
            time.as_millis(),
            fs_time.as_millis()
        );
        Ok(())
    }
}
